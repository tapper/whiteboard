webpackJsonp([4],{

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsPageModule", function() { return PointsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__points__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_components_module__ = __webpack_require__(705);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PointsPageModule = /** @class */ (function () {
    function PointsPageModule() {
    }
    PointsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__points__["a" /* PointsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__points__["a" /* PointsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], PointsPageModule);
    return PointsPageModule;
}());

//# sourceMappingURL=points.module.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__ = __webpack_require__(706);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__["a" /* LoadBarComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__["a" /* LoadBarComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadBarComponent = /** @class */ (function () {
    function LoadBarComponent(utils, zone) {
        var _this = this;
        this.utils = utils;
        this.zone = zone;
        this.full = 0;
        this.empty = 100;
        this.utils.uploadingProgress$.subscribe(function (full) {
            _this.zone.run(function () {
                _this.full = full;
                _this.empty = 100 - full;
            });
        });
    }
    LoadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'load-bar',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\components\load-bar\load-bar.html"*/'<div class="row100">\n\n  <div class="progress-bar-empty" [style.width]="empty + \'%\'"></div>\n\n  <div class="progress-bar-full" [style.width]="full + \'%\'"></div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\components\load-bar\load-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__["a" /* UtilsProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], LoadBarComponent);
    return LoadBarComponent;
}());

//# sourceMappingURL=load-bar.js.map

/***/ }),

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PointsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_filter__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_filter__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var PointsPage = /** @class */ (function () {
    function PointsPage(navCtrl, navParams, api, translate, restangular, platform, app, utils, alertCtrl, loadingCtrl, storage, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.translate = translate;
        this.restangular = restangular;
        this.platform = platform;
        this.app = app;
        this.utils = utils;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.auth = auth;
        this.user_journey = this.navParams.get('user_journey');
        this.title = this.navParams.get('title');
        this.points = [];
        this.journeys = [];
        this.newTouchpoint = '';
        this.newDuplicatedTouchpoint = '';
        this.coordinates = { lat: 0, lng: 0 };
        this.weightarr = [];
        this.isReordering = true;
        // workaround for hardware back button on Android
        this.platform.registerBackButtonAction(function () {
            _this.finish();
        });
        this.language_subscription = this.api.language$.subscribe(function (language) { return _this.language = language; });
        this.coordinates_subscription = this.utils.coordinates$.subscribe(function (coordinates) { _this.coordinates = coordinates; });
        console.log("UH : ", this.user_journey, this.title);
    }
    PointsPage.prototype.addTouchpoint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var title, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.newTouchpoint !== '' || this.newDuplicatedTouchpoint !== '')) return [3 /*break*/, 5];
                        title = this.newTouchpoint !== '' ? this.newTouchpoint : this.newDuplicatedTouchpoint;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.restangular.all('points').customPOST({ title: title, journey_id: this.user_journey.journey_id }).toPromise()];
                    case 2:
                        _a.sent();
                        this.newTouchpoint = '';
                        this.newDuplicatedTouchpoint = '';
                        return [4 /*yield*/, this.api.getJourneys()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PointsPage.prototype.finish = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, 4, 5]);
                        console.log(this.title);
                        alert_1 = this.alertCtrl.create({
                            title: this.translate.instant('ALERTS.ENDJOURNEY'),
                            message: this.translate.instant('ALERTS.ENDJOURNEY2') + this.title + this.translate.instant('ALERTS.ENDJOURNEY3'),
                            buttons: ['OK']
                        });
                        alert_1.present();
                        return [4 /*yield*/, this.utils.checkLocation()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.api.getJourneys()];
                    case 2:
                        _a.sent();
                        this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng, null, false);
                        this.api.reportJourneyPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng);
                        return [3 /*break*/, 5];
                    case 3:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [3 /*break*/, 5];
                    case 4:
                        this.navCtrl.setRoot('HomePage');
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PointsPage.prototype.goToPoint = function (point) {
        this.navCtrl.push('PointPage', { user_journey: this.user_journey, point: point });
    };
    PointsPage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // TODO: rewrite after debugging
                // await this.api.getUserJourney(3);
                console.log("ionViewDidEnter");
                this.getPoints();
                return [2 /*return*/];
            });
        });
    };
    PointsPage.prototype.getPoints = function () {
        return __awaiter(this, void 0, void 0, function () {
            var server;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
                        //loading.present();
                        console.log("JournyId : ", this.user_journey);
                        return [4 /*yield*/, this.api.getUserJourney(this.user_journey.id)];
                    case 1:
                        server = _a.sent();
                        return [4 /*yield*/, this.api.getJourneys()];
                    case 2:
                        _a.sent();
                        //loading.dismiss();
                        this.user_subscription = this.api.user_journey$.subscribe(function (journey) {
                            _this.user_journey = journey;
                            console.log("ionViewDidEnter11");
                            _this.api_subscription = _this.api.journeys$.filter(function (journey) { return journey !== null; }).subscribe(function (journeys) { return __awaiter(_this, void 0, void 0, function () {
                                var _i, _a, point, _b, _c, touchpoint;
                                var _this = this;
                                return __generator(this, function (_d) {
                                    this.journeys = journeys;
                                    this.points = this.journeys.filter(function (journey) { return journey.id === _this.user_journey.journey_id; })[0].points;
                                    for (_i = 0, _a = this.points; _i < _a.length; _i++) {
                                        point = _a[_i];
                                        point.isEditPoint = false;
                                        this.weightarr.push(point.pivot.weight);
                                        if (this.user_journey.touchpoints) {
                                            for (_b = 0, _c = this.user_journey.touchpoints; _b < _c.length; _b++) {
                                                touchpoint = _c[_b];
                                                if (touchpoint.point_id === point.id && touchpoint.user_journey_id === this.user_journey.id) {
                                                    point.touchpoint = touchpoint;
                                                }
                                            }
                                        }
                                    }
                                    console.log("Points : ", this.points, this.weightarr);
                                    return [2 /*return*/];
                                });
                            }); });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PointsPage.prototype.ionViewDidLoad = function () {
        //this.getPoints();
    };
    PointsPage.prototype.ionViewDidLeave = function () {
        for (var _i = 0, _a = this.points; _i < _a.length; _i++) {
            var point = _a[_i];
            if (point.touchpoint) {
                delete point.touchpoint;
            }
        }
        this.user_subscription.unsubscribe();
        this.language_subscription.unsubscribe();
        this.coordinates_subscription.unsubscribe();
        this.api_subscription.unsubscribe();
    };
    PointsPage.prototype.reorderItems = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isReordering = true;
                        Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* reorderArray */])(this.points, event);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, 5, 6]);
                        console.log("Order : ", this.points.map(function (p) { return p.id; }));
                        return [4 /*yield*/, this.restangular.one('journeys', this.user_journey.journey_id).customPATCH({ 'order': this.points.map(function (p) { return p.id; }) }).toPromise()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.api.getJourneys()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 4:
                        err_3 = _a.sent();
                        console.log(err_3);
                        return [3 /*break*/, 6];
                    case 5:
                        //loading.dismiss();
                        console.log(this.points);
                        return [7 /*endfinally*/];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PointsPage.prototype.editPoint = function (point) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        point.isEditPoint = point.isEditPoint == true ? false : true;
                        return [4 /*yield*/, this.restangular.all('update_points').customPOST({ point_id: point.id, point_title: point.title }).toPromise()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PointsPage.prototype.deletePoint = function (point, index) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                alert = this.alertCtrl.create({
                    title: this.translate.instant('ALERTS.DELETEAPOINT'),
                    message: '',
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            handler: function () {
                                console.log('Cancel clicked');
                            }
                        },
                        {
                            text: 'OK',
                            handler: function () {
                                _this.points.splice(index, 1);
                                _this.restangular.all('delete_points').customPOST({ point_id: point.id, point_title: point.title }).toPromise();
                                //this.getPoints();
                            }
                        }
                    ]
                });
                alert.present();
                return [2 /*return*/];
            });
        });
    };
    PointsPage.prototype.duplicatePoint = function (point, index) {
        return __awaiter(this, void 0, void 0, function () {
            var oldIndex;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.newDuplicatedTouchpoint = point.title;
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _a.sent();
                        oldIndex = this.points.length - 1;
                        return [4 /*yield*/, this.reorderItems({ from: oldIndex, to: index + 1 })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PointsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-points',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\points\points.html"*/'<ion-header>\n\n    <ion-navbar color="secondary">\n\n        <ion-title>{{title}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button color="dark" (click)="finish()">{{"POINT.END" | translate}}</button>\n\n        </ion-buttons>\n\n        <load-bar></load-bar>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content >\n\n    <ion-row class="new-touchpoint">\n\n        <ion-col col-11>\n\n            <input type="text" placeholder="{{\'POINT.ADD\' | translate}}" [(ngModel)]="newTouchpoint">\n\n        </ion-col>\n\n        <ion-col col-1 no-padding text-center>\n\n            <ion-icon name="md-send" (click)="addTouchpoint()" class="add-icon" [ngClass]="{\'icon-rtl\': language === \'he\'}"></ion-icon>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-list [reorder]="isReordering" (ionItemReorder)="reorderItems($event)">\n\n        <div ion-item no-lines *ngFor="let point of points let i=index" [ngStyle]="{\'background-color\': point.color}">\n\n            <input *ngIf="point.isEditPoint" type="text" value="{{point.title}}" [(ngModel)]="point.title" >\n\n            <div (click)="goToPoint(point)">\n\n                <div item-label *ngIf="!point.isEditPoint">{{point.title}}</div>\n\n                <!--<div item-end></div>-->\n\n            </div>\n\n            <div item-end>\n\n                <div class="outsideicon">\n\n                    <ion-icon name="md-create" color="success" *ngIf="point.user_id" (click)="editPoint(point)"></ion-icon>\n\n                </div>\n\n                <div class="outsideicon">\n\n                    <ion-icon name="ios-trash-outline" color="success" *ngIf="point.user_id" (click)="deletePoint(point,i)"></ion-icon>\n\n                </div>\n\n                <div class="outsideicon">\n\n                    <ion-icon name="checkmark" color="success" *ngIf="point.touchpoint"></ion-icon>\n\n                </div>\n\n                <div class="outsideicon">\n\n                    <ion-icon name="md-copy" color="success" (click)="duplicatePoint(point, i)"></ion-icon>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\points\points.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["a" /* Restangular */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */]])
    ], PointsPage);
    return PointsPage;
}());

//# sourceMappingURL=points.js.map

/***/ }),

/***/ 719:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(720);
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 720:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = __webpack_require__(34);
var filter_1 = __webpack_require__(721);
rxjs_1.Observable.prototype.filter = filter_1.filter;
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 721:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var operators_1 = __webpack_require__(56);
/* tslint:enable:max-line-length */
/**
 * Filter items emitted by the source Observable by only emitting those that
 * satisfy a specified predicate.
 *
 * <span class="informal">Like
 * [Array.prototype.filter()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter),
 * it only emits a value from the source if it passes a criterion function.</span>
 *
 * <img src="./img/filter.png" width="100%">
 *
 * Similar to the well-known `Array.prototype.filter` method, this operator
 * takes values from the source Observable, passes them through a `predicate`
 * function and only emits those values that yielded `true`.
 *
 * @example <caption>Emit only click events whose target was a DIV element</caption>
 * var clicks = Rx.Observable.fromEvent(document, 'click');
 * var clicksOnDivs = clicks.filter(ev => ev.target.tagName === 'DIV');
 * clicksOnDivs.subscribe(x => console.log(x));
 *
 * @see {@link distinct}
 * @see {@link distinctUntilChanged}
 * @see {@link distinctUntilKeyChanged}
 * @see {@link ignoreElements}
 * @see {@link partition}
 * @see {@link skip}
 *
 * @param {function(value: T, index: number): boolean} predicate A function that
 * evaluates each value emitted by the source Observable. If it returns `true`,
 * the value is emitted, if `false` the value is not passed to the output
 * Observable. The `index` parameter is the number `i` for the i-th source
 * emission that has happened since the subscription, starting from the number
 * `0`.
 * @param {any} [thisArg] An optional argument to determine the value of `this`
 * in the `predicate` function.
 * @return {Observable} An Observable of values from the source that were
 * allowed by the `predicate` function.
 * @method filter
 * @owner Observable
 */
function filter(predicate, thisArg) {
    return operators_1.filter(predicate, thisArg)(this);
}
exports.filter = filter;
//# sourceMappingURL=filter.js.map

/***/ })

});
//# sourceMappingURL=4.js.map