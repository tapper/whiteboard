webpackJsonp([2],{

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(707);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__background_image_background_image__ = __webpack_require__(708);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__ = __webpack_require__(709);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__ = __webpack_require__(710);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__ = __webpack_require__(711);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */],
                __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */],
                __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__["a" /* AddVideoDirective */],
                __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__["a" /* AddPictureGalleryDirective */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */],
                __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */],
                __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__["a" /* AddVideoDirective */],
                __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__["a" /* AddPictureGalleryDirective */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackgroundImageDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackgroundImageDirective = /** @class */ (function () {
    function BackgroundImageDirective(el) {
        this.el = el;
    }
    BackgroundImageDirective.prototype.ngOnChanges = function (changes) {
        for (var key in changes) {
            if (key == 'source') {
                if (this.source != null && this.source != undefined) {
                    this.el.nativeElement.style.background = "url('" + this.source + "') no-repeat top left / cover";
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], BackgroundImageDirective.prototype, "source", void 0);
    BackgroundImageDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[background-image]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], BackgroundImageDirective);
    return BackgroundImageDirective;
}());

//# sourceMappingURL=background-image.js.map

/***/ }),

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddPictureDirective = /** @class */ (function () {
    function AddPictureDirective(alertCtrl, translate, camera) {
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.camera = camera;
        this.targetImage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureDirective.prototype.onClick = function (event) {
        this.makeLogo(1);
        // let alert = this.alertCtrl.create({
        //     title: this.translate.instant("ALERTS.CHOOSESOURCE"),
        //     buttons: [
        //         {
        //             text: 'Folder',
        //             handler: () => {
        //                 this.makeLogo(0);
        //             }
        //         },
        //         {
        //             text: 'Camera',
        //             handler: () => {
        //                 this.makeLogo(1);
        //             }
        //         }
        //     ]
        // });
        // alert.present();
    };
    AddPictureDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetImage.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddPictureDirective.prototype, "targetImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddPictureDirective.prototype, "onClick", null);
    AddPictureDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-picture]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
    ], AddPictureDirective);
    return AddPictureDirective;
}());

//# sourceMappingURL=add-picture.js.map

/***/ }),

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVideoDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__ = __webpack_require__(469);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddVideoDirective = /** @class */ (function () {
    function AddVideoDirective(alertCtrl, camera, diagnostic, mediaCapture) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.mediaCapture = mediaCapture;
        this.targetVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddVideoDirective.prototype.onClick = function (event) {
        this.checkPermissions();
    };
    AddVideoDirective.prototype.selectVideo = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            mediaType: this.camera.MediaType.VIDEO,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            console.log(imageData.substr(imageData.lastIndexOf('/') + 1));
            var construct = { fullPath: '', name: '', type: '' };
            construct.fullPath = imageData;
            construct.name = imageData.substr(imageData.lastIndexOf('/') + 1);
            construct.type = 'video/mp4';
            _this.targetVideo.emit(construct);
        }, function (err) {
            console.log('err', err);
        });
    };
    AddVideoDirective.prototype.checkPermissions = function () {
        var _this = this;
        this.diagnostic.isCameraAuthorized(true).then(function (isAuthorized) {
            if (!isAuthorized) {
                _this.diagnostic.requestCameraAuthorization(true).then(function () { return _this.record(); });
            }
            else {
                _this.record();
            }
        }).catch(function (error) { return console.log('Error in camera authorization', error); });
    };
    AddVideoDirective.prototype.record = function () {
        var _this = this;
        var options = { limit: 1, duration: 30 };
        this.mediaCapture.captureVideo(options).then(function (data) {
            console.log(data);
            _this.targetVideo.emit(data[0]);
        }, function (err) { return console.error(err); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetVideo'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddVideoDirective.prototype, "targetVideo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddVideoDirective.prototype, "onClick", null);
    AddVideoDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-video]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__["a" /* MediaCapture */]])
    ], AddVideoDirective);
    return AddVideoDirective;
}());

//# sourceMappingURL=add-video.js.map

/***/ }),

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureGalleryDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddPictureGalleryDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
var AddPictureGalleryDirective = /** @class */ (function () {
    function AddPictureGalleryDirective(alertCtrl, translate, camera) {
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.camera = camera;
        this.targetImage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureGalleryDirective.prototype.onClick = function (event) {
        this.makeLogo(0);
        // let alert = this.alertCtrl.create({
        //     title: this.translate.instant("ALERTS.CHOOSESOURCE"),
        //     buttons: [
        //         {
        //             text: 'Folder',
        //             handler: () => {
        //                 this.makeLogo(0);
        //             }
        //         },
        //         {
        //             text: 'Camera',
        //             handler: () => {
        //                 this.makeLogo(1);
        //             }
        //         }
        //     ]
        // });
        // alert.present();
    };
    AddPictureGalleryDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetImage.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddPictureGalleryDirective.prototype, "targetImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddPictureGalleryDirective.prototype, "onClick", null);
    AddPictureGalleryDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-picture-gallery]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
    ], AddPictureGalleryDirective);
    return AddPictureGalleryDirective;
}());

//# sourceMappingURL=add-picture-gallery.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_config_dev__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_api_api__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, api, alertCtrl, utils, fb, auth, translate, transfer, loadingCtrl, restangular) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.utils = utils;
        this.fb = fb;
        this.auth = auth;
        this.translate = translate;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.restangular = restangular;
        this.form = this.fb.group({
            name: [''],
            phone: [''],
            email: ['', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].email]],
        });
        this.api.user$.subscribe(function (user) {
            _this.user = user;
            _this.form.setValue({ name: _this.user.name, email: _this.user.email, phone: _this.user.phone });
        });
    }
    ProfilePage.prototype.showDeletePopup = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant('ALERTS.SURE'),
            message: this.translate.instant('ALERTS.DELETEAVATAR'),
            buttons: [
                { text: 'Cancel', role: 'cancel' },
                {
                    text: 'OK',
                    handler: function () { _this.deleteAvatar(); }
                }
            ]
        });
        alert.present();
    };
    ProfilePage.prototype.deleteAvatar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, item, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _i = 0, _a = this.user.media;
                        _b.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 6];
                        item = _a[_i];
                        if (!(item.pivot.tag === 'avatar')) return [3 /*break*/, 5];
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.restangular.one('media/avatar', item.id).remove().toPromise()];
                    case 3:
                        _b.sent();
                        this.alertCtrl.create({ title: this.translate.instant('ALERTS.DELETED'), buttons: ['OK'] }).present();
                        this.api.getUser();
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _b.sent();
                        console.log(err_1);
                        return [3 /*break*/, 5];
                    case 5:
                        _i++;
                        return [3 /*break*/, 1];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.sendPicture = function (image) {
        return __awaiter(this, void 0, void 0, function () {
            var loading, fileTransfer, options, data, err_2, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('image received', image);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 7, , 8]);
                        loading = this.loadingCtrl.create({ content: 'Please wait...' });
                        loading.present();
                        fileTransfer = this.transfer.create();
                        options = {
                            fileKey: 'file',
                            mimeType: image.type,
                            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                            headers: { Authorization: "Bearer " + this.auth.token },
                            params: { type: 'image', user_id: this.user.id }
                        };
                        console.log(options);
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, 5, 6]);
                        return [4 /*yield*/, fileTransfer.upload(image.url, __WEBPACK_IMPORTED_MODULE_7__config_config_dev__["a" /* ENV */].API_ENDPOINT + '/media/avatar', options)];
                    case 3:
                        data = _a.sent();
                        console.log(JSON.parse(data.response));
                        this.api.getUser();
                        return [3 /*break*/, 6];
                    case 4:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [3 /*break*/, 6];
                    case 5:
                        loading.dismiss();
                        return [7 /*endfinally*/];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        err_3 = _a.sent();
                        console.log(err_3);
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.submit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.form.invalid) {
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.user.name = this.form.value.name;
                        this.user.phone = this.form.value.phone;
                        this.user.email = this.form.value.email;
                        return [4 /*yield*/, this.user.patch().toPromise()];
                    case 2:
                        _a.sent();
                        this.api.getUser();
                        this.alertCtrl.create({ title: this.translate.instant('ALERTS.UPDATED'), buttons: ['OK'] }).present();
                        return [3 /*break*/, 4];
                    case 3:
                        err_4 = _a.sent();
                        console.log(err_4);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\profile\profile.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="secondary">\n\n        <ion-title>{{"PROFILE.PROFILE" | translate}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="upper-div">\n\n        <div class="edit-avatar">\n\n            <button ion-button clear icon-only add-picture (targetImage)="sendPicture($event)">\n\n                <ion-icon name="create"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="delete-avatar" *ngIf="user && user.avatar">\n\n            <button ion-button clear icon-only (click)="showDeletePopup()">\n\n                <ion-icon name="close"></ion-icon>\n\n            </button>\n\n        </div>\n\n        <div class="avatar" *ngIf="user && !user.avatar" background-image [source]="\'assets/imgs/avatar.jpg\'"></div>\n\n        <div class="avatar" *ngIf="user && user.avatar" background-image [source]="user.avatar"></div>\n\n    </div>\n\n\n\n    <div class="div90 lower-div">\n\n        <form [formGroup]="form" (submit)="submit()">\n\n\n\n            <ion-list>\n\n                <ion-item no-padding>\n\n                    <ion-label fixed>{{"PROFILE.NAME" | translate}}</ion-label>\n\n                    <ion-input type="text" placeholder=\'{{"PROFILE.NAME" | translate}}\' formControlName="name"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item no-padding>\n\n                    <ion-label fixed>{{"PROFILE.EMAIL" | translate}}</ion-label>\n\n                    <ion-input type="email" placeholder=\'{{"PROFILE.EMAIL" | translate}}\' formControlName="email"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item no-padding>\n\n                    <ion-label fixed>{{"PROFILE.PHONE" | translate}}</ion-label>\n\n                    <ion-input type="tel" placeholder=\'{{"PROFILE.PHONE" | translate}}\' formControlName="phone"></ion-input>\n\n                </ion-item>\n\n\n\n                <button margin-top ion-button color="primary" block [disabled]="form.invalid" type="submit">{{"PROFILE.UPDATE" | translate}}</button>\n\n\n\n            </ion-list>\n\n\n\n        </form>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_8__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["a" /* Restangular */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

});
//# sourceMappingURL=2.js.map