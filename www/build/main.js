webpackJsonp([8],{

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var UtilsProvider = /** @class */ (function () {
    function UtilsProvider(diagnostic, platform, geolocation, alertCtrl, translate, loadingCtrl, toastCtrl) {
        this.diagnostic = diagnostic;
        this.platform = platform;
        this.geolocation = geolocation;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this._uploadingProgress = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](0);
        this.uploadingProgress$ = this._uploadingProgress.asObservable();
        this._coordinates = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({ lat: 0, lng: 0 });
        this.coordinates$ = this._coordinates.asObservable();
        this.locationCounter = 0;
        this.locationCounter1 = 0;
        this.locationCounter2 = 0;
    }
    Object.defineProperty(UtilsProvider.prototype, "uploadingProgress", {
        get: function () { return this._uploadingProgress.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UtilsProvider.prototype, "coordinates", {
        get: function () { return this._coordinates.getValue(); },
        enumerable: true,
        configurable: true
    });
    UtilsProvider.prototype.setUploadingProgress = function (value) {
        this._uploadingProgress.next(value);
    };
    UtilsProvider.prototype.isLocationAvailable = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var isAuthorized, status_1, isEnabled, isEnabled;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.diagnostic.isLocationAuthorized()];
                                case 1:
                                    isAuthorized = _a.sent();
                                    console.log('isLocationAuthorized', isAuthorized);
                                    if (!!isAuthorized) return [3 /*break*/, 6];
                                    return [4 /*yield*/, this.diagnostic.requestLocationAuthorization()];
                                case 2:
                                    status_1 = _a.sent();
                                    console.log('requestLocationAuthorization', status_1);
                                    if (!(status_1 === "GRANTED")) return [3 /*break*/, 4];
                                    return [4 /*yield*/, this.diagnostic.isLocationEnabled()];
                                case 3:
                                    isEnabled = _a.sent();
                                    console.log('isLocationEnabled', isEnabled);
                                    if (isEnabled) {
                                        resolve({ status: 'success' });
                                    }
                                    else {
                                        reject({ status: 'error', message: this.translate.instant('ALERTS.DISABLED') });
                                    }
                                    return [3 /*break*/, 5];
                                case 4:
                                    reject({ status: 'error', message: this.translate.instant('ALERTS.AUTHORIZED') });
                                    _a.label = 5;
                                case 5: return [3 /*break*/, 8];
                                case 6: return [4 /*yield*/, this.diagnostic.isLocationEnabled()];
                                case 7:
                                    isEnabled = _a.sent();
                                    console.log('isLocationEnabled', isEnabled);
                                    if (isEnabled) {
                                        resolve({ status: 'success' });
                                    }
                                    else {
                                        reject({ status: 'error', message: this.translate.instant('ALERTS.DISABLED') });
                                    }
                                    _a.label = 8;
                                case 8: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    UtilsProvider.prototype.getLocation = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var response, err_1, err_2, response, err_3;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!this.platform.is('cordova')) return [3 /*break*/, 9];
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 7, , 8]);
                                    return [4 /*yield*/, this.isLocationAvailable()];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3:
                                    _a.trys.push([3, 5, , 6]);
                                    return [4 /*yield*/, this.geolocation.getCurrentPosition({ timeout: 500 })];
                                case 4:
                                    response = _a.sent();
                                    console.log(response.coords.latitude, response.coords.longitude);
                                    this._coordinates.next({ lat: response.coords.latitude, lng: response.coords.longitude });
                                    resolve();
                                    return [3 /*break*/, 6];
                                case 5:
                                    err_1 = _a.sent();
                                    if (this.locationCounter1 == 0) {
                                        this.toastCtrl.create({
                                            message: this.translate.instant('ALERTS.CANTGET'),
                                            position: 'bottom',
                                            showCloseButton: true
                                        }).present();
                                        this.locationCounter1++;
                                    }
                                    reject();
                                    return [3 /*break*/, 6];
                                case 6: return [3 /*break*/, 8];
                                case 7:
                                    err_2 = _a.sent();
                                    console.log(err_2);
                                    if (this.locationCounter1 == 0) {
                                        this.toastCtrl.create({ message: err_2.message, position: 'bottom', showCloseButton: true }).present();
                                        this.locationCounter1++;
                                    }
                                    reject();
                                    return [3 /*break*/, 8];
                                case 8: return [3 /*break*/, 12];
                                case 9:
                                    _a.trys.push([9, 11, , 12]);
                                    return [4 /*yield*/, this.geolocation.getCurrentPosition({ maximumAge: 10000, timeout: 5000 })];
                                case 10:
                                    response = _a.sent();
                                    console.log(response.coords.latitude, response.coords.longitude);
                                    this._coordinates.next({ lat: response.coords.latitude, lng: response.coords.longitude });
                                    resolve();
                                    return [3 /*break*/, 12];
                                case 11:
                                    err_3 = _a.sent();
                                    this.toastCtrl.create({ message: err_3.message, position: 'bottom', showCloseButton: true }).present();
                                    this.locationCounter1++;
                                    reject();
                                    return [3 /*break*/, 12];
                                case 12: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    UtilsProvider.prototype.checkLocation = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var err_4, alert_1;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, 3, 4]);
                                    return [4 /*yield*/, this.getLocation()];
                                case 1:
                                    _a.sent();
                                    resolve();
                                    return [3 /*break*/, 4];
                                case 2:
                                    err_4 = _a.sent();
                                    console.log(err_4);
                                    if (this.locationCounter == 0) {
                                        alert_1 = this.alertCtrl.create({
                                            title: this.translate.instant('ALERTS.NOLOCATION'),
                                            message: this.translate.instant('ALERTS.CHECK'),
                                            buttons: [
                                                {
                                                    text: this.translate.instant('ALERTS.CONTINUE'),
                                                    handler: function () { resolve(); }
                                                },
                                                {
                                                    text: this.translate.instant('ALERTS.TRYAGAIN'),
                                                    handler: function () {
                                                        _this.getLocation().then(function (success) { resolve(); }, function (error) { reject(); });
                                                    }
                                                },
                                            ]
                                        });
                                        alert_1.present();
                                        this.locationCounter++;
                                    }
                                    else {
                                        resolve();
                                    }
                                    return [3 /*break*/, 4];
                                case 3: return [7 /*endfinally*/];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    UtilsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* ToastController */]])
    ], UtilsProvider);
    return UtilsProvider;
}());

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ApiProvider = /** @class */ (function () {
    function ApiProvider(restangular, loadingCtrl, storage) {
        this.restangular = restangular;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this._language = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.language$ = this._language.asObservable();
        this._journeys = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.journeys$ = this._journeys.asObservable();
        this._user_journey = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.user_journey$ = this._user_journey.asObservable();
        this._user = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.user$ = this._user.asObservable();
    }
    Object.defineProperty(ApiProvider.prototype, "language", {
        get: function () { return this._language.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "journeys", {
        get: function () { return this._journeys.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "user_journey", {
        get: function () { return this._user_journey.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "user", {
        get: function () { return this._user.getValue(); },
        enumerable: true,
        configurable: true
    });
    ApiProvider.prototype.getJourneys = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var journeys, err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, 3, 4]);
                                    return [4 /*yield*/, this.restangular.all('journeys').getList().toPromise()];
                                case 1:
                                    journeys = _a.sent();
                                    this._journeys.next(journeys);
                                    resolve(this.journeys);
                                    return [3 /*break*/, 4];
                                case 2:
                                    err_1 = _a.sent();
                                    reject(err_1);
                                    console.log(err_1);
                                    return [3 /*break*/, 4];
                                case 3: return [7 /*endfinally*/];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ApiProvider.prototype.startJourney = function (journey_id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var user_journey, err_2;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    console.log(journey_id);
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, this.restangular.all('user_journeys').customPOST({ journey_id: journey_id }).toPromise()];
                                case 2:
                                    user_journey = _a.sent();
                                    this._user_journey.next(user_journey);
                                    resolve(user_journey);
                                    return [3 /*break*/, 4];
                                case 3:
                                    err_2 = _a.sent();
                                    console.log(err_2);
                                    return [3 /*break*/, 4];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ApiProvider.prototype.getUserJourney = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var user_journey, err_3;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, this.restangular.one('user_journeys', id).get().toPromise()];
                                case 1:
                                    user_journey = _a.sent();
                                    this._user_journey.next(user_journey);
                                    resolve(user_journey);
                                    return [3 /*break*/, 3];
                                case 2:
                                    err_3 = _a.sent();
                                    console.log(err_3);
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ApiProvider.prototype.getUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var id, user, err_4;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 3, , 4]);
                                    return [4 /*yield*/, this.storage.get('id')];
                                case 1:
                                    id = _a.sent();
                                    return [4 /*yield*/, this.restangular.one('users', id).get().toPromise()];
                                case 2:
                                    user = _a.sent();
                                    this._user.next(user);
                                    resolve(user);
                                    return [3 /*break*/, 4];
                                case 3:
                                    err_4 = _a.sent();
                                    console.log(err_4);
                                    return [3 /*break*/, 4];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     *
     * @param {string} language
     * @returns {Promise<void>}
     */
    ApiProvider.prototype.setLanguage = function (language) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var err_5;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    this._language.next(language);
                                    return [4 /*yield*/, this.storage.set('language', this._language.getValue())];
                                case 1:
                                    _a.sent();
                                    _a.label = 2;
                                case 2:
                                    _a.trys.push([2, 4, , 5]);
                                    return [4 /*yield*/, this.restangular.all('lang').customGET('', { language: language }).toPromise()];
                                case 3:
                                    _a.sent();
                                    resolve();
                                    return [3 /*break*/, 5];
                                case 4:
                                    err_5 = _a.sent();
                                    console.log(err_5);
                                    reject();
                                    return [3 /*break*/, 5];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    ApiProvider.prototype.reportTouchpointPeriod = function (time, user_journey, lat, lng, touchpoint, next) {
        if (touchpoint === void 0) { touchpoint = null; }
        if (next === void 0) { next = true; }
        return __awaiter(this, void 0, void 0, function () {
            var err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('user_journeys', user_journey)
                                .all('touchpoint_periods')
                                .customPOST({ touchpoint_id: touchpoint, time: time, lat: lat, lng: lng, next: next })
                                .toPromise()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_6 = _a.sent();
                        console.log(err_6);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ApiProvider.prototype.reportJourneyPeriod = function (time, user_journey, lat, lng) {
        return __awaiter(this, void 0, void 0, function () {
            var err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('user_journeys', user_journey)
                                .all('journey_periods')
                                .customPOST({ time: time, lat: lat, lng: lng })
                                .toPromise()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_7 = _a.sent();
                        console.log(err_7);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */])
    ], ApiProvider.prototype, "nav", void 0);
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["a" /* Restangular */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_status__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var AuthProvider = /** @class */ (function () {
    function AuthProvider(storage, app) {
        this.storage = storage;
        this.app = app;
        this._token = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.token$ = this._token.asObservable();
        this._id = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.id$ = this._id.asObservable();
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.status$ = this._status.asObservable();
    }
    Object.defineProperty(AuthProvider.prototype, "token", {
        get: function () { return this._token.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthProvider.prototype, "id", {
        get: function () { return this._id.getValue(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthProvider.prototype, "status", {
        get: function () { return this._status.getValue(); },
        enumerable: true,
        configurable: true
    });
    AuthProvider.prototype.checkTokenOnStart = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var token, id, err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 5, , 6]);
                                    return [4 /*yield*/, this.storage.get('token')];
                                case 1:
                                    token = _a.sent();
                                    if (!!token) return [3 /*break*/, 2];
                                    this._status.next(__WEBPACK_IMPORTED_MODULE_2__auth_status__["a" /* AuthStatuses */].NOT_AUTHENTICATED);
                                    return [3 /*break*/, 4];
                                case 2:
                                    this._token.next(token);
                                    this._status.next(__WEBPACK_IMPORTED_MODULE_2__auth_status__["a" /* AuthStatuses */].AUTHENTICATED);
                                    return [4 /*yield*/, this.storage.get('id')];
                                case 3:
                                    id = _a.sent();
                                    this._id.next(id);
                                    _a.label = 4;
                                case 4:
                                    resolve();
                                    return [3 /*break*/, 6];
                                case 5:
                                    err_1 = _a.sent();
                                    reject(err_1);
                                    return [3 /*break*/, 6];
                                case 6: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     *
     * @param {string} token
     * @returns {Promise<void>}
     */
    AuthProvider.prototype.setToken = function (token) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            try {
                                this._token.next(token);
                                this.storage.set('token', this._token.getValue());
                                resolve();
                            }
                            catch (err) {
                                console.log(err);
                                reject(err);
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    AuthProvider.prototype.setId = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            try {
                                this._id.next(id);
                                this.storage.set('id', this._id.getValue());
                                resolve();
                            }
                            catch (err) {
                                console.log(err);
                                reject(err);
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    /**
     *
     * @returns {Promise<void>}
     */
    AuthProvider.prototype.setStatus = function (status) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            try {
                                this._status.next(status);
                                resolve();
                            }
                            catch (err) {
                                console.log(err);
                                reject(err);
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    /**
     * Logout clearing all.
     *
     * @returns {Promise<void>}
     */
    AuthProvider.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    this.setToken(null);
                                    return [4 /*yield*/, this.storage.clear()];
                                case 1:
                                    _a.sent();
                                    this._status.next(__WEBPACK_IMPORTED_MODULE_2__auth_status__["a" /* AuthStatuses */].NOT_AUTHENTICATED);
                                    resolve();
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* App */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthStatuses; });
var AuthStatuses;
(function (AuthStatuses) {
    AuthStatuses[AuthStatuses["NOT_AUTHENTICATED"] = 0] = "NOT_AUTHENTICATED";
    AuthStatuses[AuthStatuses["AUTHENTICATED"] = 1] = "AUTHENTICATED";
})(AuthStatuses || (AuthStatuses = {}));
//# sourceMappingURL=auth.status.js.map

/***/ }),

/***/ 225:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 225;

/***/ }),

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/home/home.module": [
		697,
		1
	],
	"../pages/journey/journey.module": [
		699,
		3
	],
	"../pages/journeys/journeys.module": [
		698,
		5
	],
	"../pages/login/login.module": [
		700,
		7
	],
	"../pages/point/point.module": [
		701,
		0
	],
	"../pages/points/points.module": [
		703,
		4
	],
	"../pages/popup/popup.module": [
		702,
		6
	],
	"../pages/profile/profile.module": [
		704,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 267;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENV; });
var ENV = {
    API_ENDPOINT: 'http://whiteboard.kartisim.co.il/api/v1',
};
//# sourceMappingURL=config.dev.js.map

/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(476);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(589);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* unused harmony export RestangularConfigFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_restangular__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_config_dev__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_auth__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_media_capture__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_utils_utils__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angular2_moment__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_media__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_common_http__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ngx_translate_http_loader__ = __webpack_require__(696);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_keyboard__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
























function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_22__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
function RestangularConfigFactory(RestangularProvider, auth, alertCtrl) {
    var _this = this;
    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl(__WEBPACK_IMPORTED_MODULE_8__config_config_dev__["a" /* ENV */].API_ENDPOINT);
    // Setting up attachment of header token
    RestangularProvider.addFullRequestInterceptor(function (element, operation, path, url, headers, params) {
        return {
            headers: Object.assign({}, headers, { Authorization: 'Bearer ' + ("" + auth.token) })
        };
    });
    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
        if (data) {
            console.log(url, data.data, operation);
        }
        return data.data;
    });
    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(function (response, subject, responseHandler) { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            console.log('ErrorInterceptor', response);
            if (response.data && !response.success) {
                if (response.data.error && response.data.error.message) {
                    alertCtrl.create({ title: response.data.error.message, buttons: ['OK'] }).present();
                    console.log("Er1");
                }
                else {
                    alertCtrl.create({ title: 'Server error!', buttons: ['OK'] }).present();
                    console.log("Er2");
                }
            }
            return [2 /*return*/];
        });
    }); });
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], { iconMode: 'md', mode: 'md' }, {
                    links: [
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/journeys/journeys.module#JourneysPageModule', name: 'JourneysPage', segment: 'journeys', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/journey/journey.module#JourneyPageModule', name: 'JourneyPage', segment: 'journey', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/point/point.module#PointPageModule', name: 'PointPage', segment: 'point', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popup/popup.module#PopupPageModule', name: 'PopupPage', segment: 'popup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/points/points.module#PointsPageModule', name: 'PointsPage', segment: 'points', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7_ngx_restangular__["b" /* RestangularModule */].forRoot([__WEBPACK_IMPORTED_MODULE_9__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]], RestangularConfigFactory),
                __WEBPACK_IMPORTED_MODULE_16_angular2_moment__["MomentModule"],
                __WEBPACK_IMPORTED_MODULE_21__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_20__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_20__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_21__angular_common_http__["b" /* HttpClient */]]
                    }
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_9__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_15__providers_utils_utils__["a" /* UtilsProvider */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_media__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_keyboard__["a" /* Keyboard */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 676:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 293,
	"./af.js": 293,
	"./ar": 294,
	"./ar-dz": 295,
	"./ar-dz.js": 295,
	"./ar-kw": 296,
	"./ar-kw.js": 296,
	"./ar-ly": 297,
	"./ar-ly.js": 297,
	"./ar-ma": 298,
	"./ar-ma.js": 298,
	"./ar-sa": 299,
	"./ar-sa.js": 299,
	"./ar-tn": 300,
	"./ar-tn.js": 300,
	"./ar.js": 294,
	"./az": 301,
	"./az.js": 301,
	"./be": 302,
	"./be.js": 302,
	"./bg": 303,
	"./bg.js": 303,
	"./bm": 304,
	"./bm.js": 304,
	"./bn": 305,
	"./bn.js": 305,
	"./bo": 306,
	"./bo.js": 306,
	"./br": 307,
	"./br.js": 307,
	"./bs": 308,
	"./bs.js": 308,
	"./ca": 309,
	"./ca.js": 309,
	"./cs": 310,
	"./cs.js": 310,
	"./cv": 311,
	"./cv.js": 311,
	"./cy": 312,
	"./cy.js": 312,
	"./da": 313,
	"./da.js": 313,
	"./de": 314,
	"./de-at": 315,
	"./de-at.js": 315,
	"./de-ch": 316,
	"./de-ch.js": 316,
	"./de.js": 314,
	"./dv": 317,
	"./dv.js": 317,
	"./el": 318,
	"./el.js": 318,
	"./en-au": 319,
	"./en-au.js": 319,
	"./en-ca": 320,
	"./en-ca.js": 320,
	"./en-gb": 321,
	"./en-gb.js": 321,
	"./en-ie": 322,
	"./en-ie.js": 322,
	"./en-il": 323,
	"./en-il.js": 323,
	"./en-nz": 324,
	"./en-nz.js": 324,
	"./eo": 325,
	"./eo.js": 325,
	"./es": 326,
	"./es-do": 327,
	"./es-do.js": 327,
	"./es-us": 328,
	"./es-us.js": 328,
	"./es.js": 326,
	"./et": 329,
	"./et.js": 329,
	"./eu": 330,
	"./eu.js": 330,
	"./fa": 331,
	"./fa.js": 331,
	"./fi": 332,
	"./fi.js": 332,
	"./fo": 333,
	"./fo.js": 333,
	"./fr": 334,
	"./fr-ca": 335,
	"./fr-ca.js": 335,
	"./fr-ch": 336,
	"./fr-ch.js": 336,
	"./fr.js": 334,
	"./fy": 337,
	"./fy.js": 337,
	"./gd": 338,
	"./gd.js": 338,
	"./gl": 339,
	"./gl.js": 339,
	"./gom-latn": 340,
	"./gom-latn.js": 340,
	"./gu": 341,
	"./gu.js": 341,
	"./he": 342,
	"./he.js": 342,
	"./hi": 343,
	"./hi.js": 343,
	"./hr": 344,
	"./hr.js": 344,
	"./hu": 345,
	"./hu.js": 345,
	"./hy-am": 346,
	"./hy-am.js": 346,
	"./id": 347,
	"./id.js": 347,
	"./is": 348,
	"./is.js": 348,
	"./it": 349,
	"./it.js": 349,
	"./ja": 350,
	"./ja.js": 350,
	"./jv": 351,
	"./jv.js": 351,
	"./ka": 352,
	"./ka.js": 352,
	"./kk": 353,
	"./kk.js": 353,
	"./km": 354,
	"./km.js": 354,
	"./kn": 355,
	"./kn.js": 355,
	"./ko": 356,
	"./ko.js": 356,
	"./ky": 357,
	"./ky.js": 357,
	"./lb": 358,
	"./lb.js": 358,
	"./lo": 359,
	"./lo.js": 359,
	"./lt": 360,
	"./lt.js": 360,
	"./lv": 361,
	"./lv.js": 361,
	"./me": 362,
	"./me.js": 362,
	"./mi": 363,
	"./mi.js": 363,
	"./mk": 364,
	"./mk.js": 364,
	"./ml": 365,
	"./ml.js": 365,
	"./mr": 366,
	"./mr.js": 366,
	"./ms": 367,
	"./ms-my": 368,
	"./ms-my.js": 368,
	"./ms.js": 367,
	"./mt": 369,
	"./mt.js": 369,
	"./my": 370,
	"./my.js": 370,
	"./nb": 371,
	"./nb.js": 371,
	"./ne": 372,
	"./ne.js": 372,
	"./nl": 373,
	"./nl-be": 374,
	"./nl-be.js": 374,
	"./nl.js": 373,
	"./nn": 375,
	"./nn.js": 375,
	"./pa-in": 376,
	"./pa-in.js": 376,
	"./pl": 377,
	"./pl.js": 377,
	"./pt": 378,
	"./pt-br": 379,
	"./pt-br.js": 379,
	"./pt.js": 378,
	"./ro": 380,
	"./ro.js": 380,
	"./ru": 381,
	"./ru.js": 381,
	"./sd": 382,
	"./sd.js": 382,
	"./se": 383,
	"./se.js": 383,
	"./si": 384,
	"./si.js": 384,
	"./sk": 385,
	"./sk.js": 385,
	"./sl": 386,
	"./sl.js": 386,
	"./sq": 387,
	"./sq.js": 387,
	"./sr": 388,
	"./sr-cyrl": 389,
	"./sr-cyrl.js": 389,
	"./sr.js": 388,
	"./ss": 390,
	"./ss.js": 390,
	"./sv": 391,
	"./sv.js": 391,
	"./sw": 392,
	"./sw.js": 392,
	"./ta": 393,
	"./ta.js": 393,
	"./te": 394,
	"./te.js": 394,
	"./tet": 395,
	"./tet.js": 395,
	"./tg": 396,
	"./tg.js": 396,
	"./th": 397,
	"./th.js": 397,
	"./tl-ph": 398,
	"./tl-ph.js": 398,
	"./tlh": 399,
	"./tlh.js": 399,
	"./tr": 400,
	"./tr.js": 400,
	"./tzl": 401,
	"./tzl.js": 401,
	"./tzm": 402,
	"./tzm-latn": 403,
	"./tzm-latn.js": 403,
	"./tzm.js": 402,
	"./ug-cn": 404,
	"./ug-cn.js": 404,
	"./uk": 405,
	"./uk.js": 405,
	"./ur": 406,
	"./ur.js": 406,
	"./uz": 407,
	"./uz-latn": 408,
	"./uz-latn.js": 408,
	"./uz.js": 407,
	"./vi": 409,
	"./vi.js": 409,
	"./x-pseudo": 410,
	"./x-pseudo.js": 410,
	"./yo": 411,
	"./yo.js": 411,
	"./zh-cn": 412,
	"./zh-cn.js": 412,
	"./zh-hk": 413,
	"./zh-hk.js": 413,
	"./zh-tw": 414,
	"./zh-tw.js": 414
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 676;

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth_status__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, api, utils, translate, storage, keyboard, alertCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.api = api;
        this.utils = utils;
        this.translate = translate;
        this.storage = storage;
        this.keyboard = keyboard;
        this.alertCtrl = alertCtrl;
        this.rootPage = 'HomePage';
        this.showFooter = true;
        this.imagesArray = [];
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var language, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _a.sent();
                        this.statusBar.styleDefault();
                        return [4 /*yield*/, this.storage.get('language')];
                    case 2:
                        language = _a.sent();
                        if (language == null) {
                            language = 'en';
                        }
                        console.log(language);
                        this.api.setLanguage(language);
                        this.translate.setDefaultLang(language);
                        this.platform.setDir(language === 'en' ? 'ltr' : 'rtl', true);
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.auth.checkTokenOnStart()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        console.log('checkTokenOnStart', err_1);
                        return [3 /*break*/, 6];
                    case 6:
                        this.auth.status$.subscribe(function (status) { return __awaiter(_this, void 0, void 0, function () {
                            var _a, a, _i, a_1, image, obj;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        console.log("Status : ", status, __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth_status__["a" /* AuthStatuses */].AUTHENTICATED);
                                        _a = status;
                                        switch (_a) {
                                            case __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth_status__["a" /* AuthStatuses */].AUTHENTICATED: return [3 /*break*/, 1];
                                            case __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth_status__["a" /* AuthStatuses */].NOT_AUTHENTICATED: return [3 /*break*/, 5];
                                        }
                                        return [3 /*break*/, 7];
                                    case 1:
                                        this.utils.getLocation();
                                        console.log("LLL1");
                                        return [4 /*yield*/, this.api.getJourneys()];
                                    case 2:
                                        a = _b.sent();
                                        for (_i = 0, a_1 = a; _i < a_1.length; _i++) {
                                            image = a_1[_i];
                                            obj = {
                                                id: image['id'],
                                                image: image['imagetwo']
                                            };
                                            this.imagesArray.push(obj);
                                        }
                                        console.log("II ", this.imagesArray);
                                        return [4 /*yield*/, this.storage.set('journeys', this.imagesArray)];
                                    case 3:
                                        _b.sent();
                                        console.log("TTT : ", a);
                                        return [4 /*yield*/, this.api.getUser()];
                                    case 4:
                                        _b.sent();
                                        // this.nav.setRoot('HomePage');
                                        this.splashScreen.hide();
                                        return [3 /*break*/, 7];
                                    case 5: return [4 /*yield*/, this.nav.setRoot('LoginPage')];
                                    case 6:
                                        _b.sent();
                                        this.splashScreen.hide();
                                        return [3 /*break*/, 7];
                                    case 7: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.logout = function () {
        this.auth.logout();
        this.nav.setRoot('LoginPage');
    };
    MyApp.prototype.changeLanguage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var lang, alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('language')];
                    case 1:
                        lang = _a.sent();
                        alert = this.alertCtrl.create();
                        alert.setTitle("Select language");
                        alert.addInput({ type: 'radio', label: 'English', value: 'en', checked: lang === 'en' });
                        alert.addInput({ type: 'radio', label: 'עברית', value: 'he', checked: lang === 'he' });
                        alert.addButton('Cancel');
                        alert.addButton({
                            text: 'OK',
                            handler: function (data) {
                                _this.api.setLanguage(data).then(function (response) {
                                    _this.api.getJourneys();
                                });
                                _this.translate.setDefaultLang(data);
                                _this.platform.setDir(data === 'en' ? 'ltr' : 'rtl', true);
                            }
                        });
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.goToProfile = function () {
        this.nav.push('ProfilePage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar color="secondary">\n\n      <ion-title>{{"GENERAL.MENU" | translate}}</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <!--<button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">-->\n\n        <!--{{p.title}}-->\n\n      <!--</button>-->\n\n      <button menuClose ion-item (click)="changeLanguage()">\n\n        Change language\n\n      </button>\n\n      <button menuClose ion-item (click)="goToProfile()">\n\n        {{"GENERAL.PROFILE" | translate}}\n\n      </button>\n\n      <button menuClose ion-item (click)="logout()">\n\n        {{"GENERAL.LOGOUT" | translate}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[475]);
//# sourceMappingURL=main.js.map