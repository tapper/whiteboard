webpackJsonp([0],{

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointPageModule", function() { return PointPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__point__ = __webpack_require__(716);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_components_module__ = __webpack_require__(705);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(707);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var PointPageModule = /** @class */ (function () {
    function PointPageModule() {
    }
    PointPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__point__["a" /* PointPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__point__["a" /* PointPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], PointPageModule);
    return PointPageModule;
}());

//# sourceMappingURL=point.module.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__ = __webpack_require__(706);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__["a" /* LoadBarComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__load_bar_load_bar__["a" /* LoadBarComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadBarComponent = /** @class */ (function () {
    function LoadBarComponent(utils, zone) {
        var _this = this;
        this.utils = utils;
        this.zone = zone;
        this.full = 0;
        this.empty = 100;
        this.utils.uploadingProgress$.subscribe(function (full) {
            _this.zone.run(function () {
                _this.full = full;
                _this.empty = 100 - full;
            });
        });
    }
    LoadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'load-bar',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\components\load-bar\load-bar.html"*/'<div class="row100">\n\n  <div class="progress-bar-empty" [style.width]="empty + \'%\'"></div>\n\n  <div class="progress-bar-full" [style.width]="full + \'%\'"></div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\components\load-bar\load-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__["a" /* UtilsProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], LoadBarComponent);
    return LoadBarComponent;
}());

//# sourceMappingURL=load-bar.js.map

/***/ }),

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__background_image_background_image__ = __webpack_require__(708);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__ = __webpack_require__(709);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__ = __webpack_require__(710);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__ = __webpack_require__(711);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */],
                __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */],
                __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__["a" /* AddVideoDirective */],
                __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__["a" /* AddPictureGalleryDirective */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__background_image_background_image__["a" /* BackgroundImageDirective */],
                __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */],
                __WEBPACK_IMPORTED_MODULE_3__add_video_add_video__["a" /* AddVideoDirective */],
                __WEBPACK_IMPORTED_MODULE_4__add_picture_gallery_add_picture_gallery__["a" /* AddPictureGalleryDirective */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 708:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackgroundImageDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackgroundImageDirective = /** @class */ (function () {
    function BackgroundImageDirective(el) {
        this.el = el;
    }
    BackgroundImageDirective.prototype.ngOnChanges = function (changes) {
        for (var key in changes) {
            if (key == 'source') {
                if (this.source != null && this.source != undefined) {
                    this.el.nativeElement.style.background = "url('" + this.source + "') no-repeat top left / cover";
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], BackgroundImageDirective.prototype, "source", void 0);
    BackgroundImageDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[background-image]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], BackgroundImageDirective);
    return BackgroundImageDirective;
}());

//# sourceMappingURL=background-image.js.map

/***/ }),

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddPictureDirective = /** @class */ (function () {
    function AddPictureDirective(alertCtrl, translate, camera) {
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.camera = camera;
        this.targetImage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureDirective.prototype.onClick = function (event) {
        this.makeLogo(1);
        // let alert = this.alertCtrl.create({
        //     title: this.translate.instant("ALERTS.CHOOSESOURCE"),
        //     buttons: [
        //         {
        //             text: 'Folder',
        //             handler: () => {
        //                 this.makeLogo(0);
        //             }
        //         },
        //         {
        //             text: 'Camera',
        //             handler: () => {
        //                 this.makeLogo(1);
        //             }
        //         }
        //     ]
        // });
        // alert.present();
    };
    AddPictureDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetImage.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddPictureDirective.prototype, "targetImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddPictureDirective.prototype, "onClick", null);
    AddPictureDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-picture]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
    ], AddPictureDirective);
    return AddPictureDirective;
}());

//# sourceMappingURL=add-picture.js.map

/***/ }),

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVideoDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__ = __webpack_require__(469);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddVideoDirective = /** @class */ (function () {
    function AddVideoDirective(alertCtrl, camera, diagnostic, mediaCapture) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.mediaCapture = mediaCapture;
        this.targetVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddVideoDirective.prototype.onClick = function (event) {
        this.checkPermissions();
    };
    AddVideoDirective.prototype.selectVideo = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            mediaType: this.camera.MediaType.VIDEO,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            console.log(imageData.substr(imageData.lastIndexOf('/') + 1));
            var construct = { fullPath: '', name: '', type: '' };
            construct.fullPath = imageData;
            construct.name = imageData.substr(imageData.lastIndexOf('/') + 1);
            construct.type = 'video/mp4';
            _this.targetVideo.emit(construct);
        }, function (err) {
            console.log('err', err);
        });
    };
    AddVideoDirective.prototype.checkPermissions = function () {
        var _this = this;
        this.diagnostic.isCameraAuthorized(true).then(function (isAuthorized) {
            if (!isAuthorized) {
                _this.diagnostic.requestCameraAuthorization(true).then(function () { return _this.record(); });
            }
            else {
                _this.record();
            }
        }).catch(function (error) { return console.log('Error in camera authorization', error); });
    };
    AddVideoDirective.prototype.record = function () {
        var _this = this;
        var options = { limit: 1, duration: 30 };
        this.mediaCapture.captureVideo(options).then(function (data) {
            console.log(data);
            _this.targetVideo.emit(data[0]);
        }, function (err) { return console.error(err); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetVideo'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddVideoDirective.prototype, "targetVideo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddVideoDirective.prototype, "onClick", null);
    AddVideoDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-video]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__["a" /* MediaCapture */]])
    ], AddVideoDirective);
    return AddVideoDirective;
}());

//# sourceMappingURL=add-video.js.map

/***/ }),

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureGalleryDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddPictureGalleryDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
var AddPictureGalleryDirective = /** @class */ (function () {
    function AddPictureGalleryDirective(alertCtrl, translate, camera) {
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.camera = camera;
        this.targetImage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureGalleryDirective.prototype.onClick = function (event) {
        this.makeLogo(0);
        // let alert = this.alertCtrl.create({
        //     title: this.translate.instant("ALERTS.CHOOSESOURCE"),
        //     buttons: [
        //         {
        //             text: 'Folder',
        //             handler: () => {
        //                 this.makeLogo(0);
        //             }
        //         },
        //         {
        //             text: 'Camera',
        //             handler: () => {
        //                 this.makeLogo(1);
        //             }
        //         }
        //     ]
        // });
        // alert.present();
    };
    AddPictureGalleryDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetImage.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetImage'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddPictureGalleryDirective.prototype, "targetImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddPictureGalleryDirective.prototype, "onClick", null);
    AddPictureGalleryDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-picture-gallery]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
    ], AddPictureGalleryDirective);
    return AddPictureGalleryDirective;
}());

//# sourceMappingURL=add-picture-gallery.js.map

/***/ }),

/***/ 716:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PointPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_config_dev__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_media__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var PointPage = /** @class */ (function () {
    function PointPage(navCtrl, navParams, restangular, api, transfer, auth, loadingCtrl, utils, zone, alertCtrl, media, platform, translate, element, file, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.api = api;
        this.transfer = transfer;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.media = media;
        this.platform = platform;
        this.translate = translate;
        this.element = element;
        this.file = file;
        this.storage = storage;
        this.coordinates = { lat: 0, lng: 0 };
        this.point = this.navParams.get('point');
        this.progress = 0;
        this.recording = false;
        this.stars = [false, false, false, false, false];
        this.text = '';
        this.uploading = false;
        this.apiSubscribe = this.api.user_journey$.subscribe(function (journey) {
            if (journey) {
                _this.user_journey = journey;
                console.log("UserJ : ", _this.user_journey);
                if (_this.user_journey.touchpoints && _this.user_journey.touchpoints.length) {
                    for (var _i = 0, _a = _this.user_journey.touchpoints; _i < _a.length; _i++) {
                        var touchpoint = _a[_i];
                        if (touchpoint.point_id === _this.point.id) {
                            _this.touchpoint = touchpoint;
                        }
                    }
                }
            }
            _this.language_subscription = _this.api.language$.subscribe(function (language) { return _this.language = language; });
            _this.getCurrentImage();
        });
        // this.api.journeys$.subscribe(journeys => {
        //     this.journeys = journeys;
        //     for (let journey of this.journeys){
        //         if (journey.user_journeys && journey.user_journeys.length){
        //             this.hasHistory = true;
        //             break;
        //         }
        //     }
        // });
        this.platform.registerBackButtonAction(function () {
            _this.goBack();
            return;
        });
        this.uploadSubscribe = this.utils.uploadingProgress$.subscribe(function (progress) { _this.zone.run(function () { _this.progress = progress; }); });
        this.coordinatesSubscribe = this.utils.coordinates$.subscribe(function (coordinates) { _this.coordinates = coordinates; });
    }
    PointPage.prototype.ionViewDidLeave = function () {
        this.apiSubscribe.unsubscribe();
        this.uploadSubscribe.unsubscribe();
        this.coordinatesSubscribe.unsubscribe();
        if (this.audioSuccessSubscribe)
            this.audioSuccessSubscribe.unsubscribe();
        if (this.audioErrorSubscribe)
            this.audioErrorSubscribe.unsubscribe();
    };
    PointPage.prototype.goBack = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.touchpoint) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.utils.checkLocation()];
                    case 1:
                        _a.sent();
                        this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng);
                        _a.label = 2;
                    case 2:
                        this.navCtrl.pop();
                        return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.autoSize = function (pixels) {
        if (pixels === void 0) { pixels = 0; }
        var textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        if (pixels === 0) {
            textArea.style.height = textArea.scrollHeight + 'px';
        }
        else {
            textArea.style.height = pixels + 'px';
        }
        return;
    };
    PointPage.prototype.editTouchpoint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var err_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, this.utils.checkLocation()];
                                case 1:
                                    _a.sent();
                                    this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng, this.touchpoint.id);
                                    resolve();
                                    return [3 /*break*/, 3];
                                case 2:
                                    err_1 = _a.sent();
                                    console.log(err_1);
                                    reject();
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    PointPage.prototype.addTouchpoint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var payload, _a, err_2;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _b.trys.push([0, 4, , 5]);
                                    return [4 /*yield*/, this.utils.checkLocation()];
                                case 1:
                                    _b.sent();
                                    payload = {
                                        user_journey_id: this.user_journey.id,
                                        point_id: this.point.id,
                                        lat: this.coordinates.lat,
                                        lng: this.coordinates.lng
                                    };
                                    _a = this;
                                    return [4 /*yield*/, this.restangular.all('touchpoints').customPOST(payload).toPromise()];
                                case 2:
                                    _a.touchpoint = _b.sent();
                                    return [4 /*yield*/, this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng, this.touchpoint.id)];
                                case 3:
                                    _b.sent();
                                    resolve();
                                    return [3 /*break*/, 5];
                                case 4:
                                    err_2 = _b.sent();
                                    console.log(err_2);
                                    reject();
                                    return [3 /*break*/, 5];
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    PointPage.prototype.ionViewWillEnter = function () {
        this.user_journey = this.navParams.get('user_journey');
        this.point = this.navParams.get('point');
        this.touchpoint = this.point.touchpoint;
        if (this.touchpoint && this.touchpoint.mark) {
            for (var i = 0; i < this.stars.length; i++) {
                if (i < this.touchpoint.mark) {
                    this.stars[i] = true;
                }
                else if (i >= this.touchpoint.mark && i < this.stars.length) {
                    this.stars[i] = false;
                }
            }
        }
        console.log(this.touchpoint);
        this.content.scrollToBottom();
    };
    PointPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.navBar.backButtonClick = function () {
            event.stopPropagation();
            _this.goBack();
        };
        if (this.touchpoint) {
            this.editTouchpoint();
        }
    };
    PointPage.prototype.rate = function (index) {
        for (var i = 0; i < this.stars.length; i++) {
            if (i <= index) {
                this.stars[i] = true;
            }
            else if (i > index && i < this.stars.length) {
                this.stars[i] = false;
            }
        }
        this.sendRate(index + 1);
    };
    PointPage.prototype.record = function () {
        var _this = this;
        if (this.platform.is('ios')) {
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
            this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
        }
        else if (this.platform.is('android')) {
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
            this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
        }
        this.audio = this.media.create(this.filePath);
        this.audio.startRecord();
        this.recording = true;
        this.audioSuccessSubscribe = this.audio.onSuccess.subscribe(function () {
            console.log('Action is successful');
            _this.sendAudio();
        });
        this.audioErrorSubscribe = this.audio.onError.subscribe(function (error) {
            console.log('Error!', error);
            _this.alertCtrl.create({ title: _this.translate.instant('ALERTS.RECORD'), buttons: ['OK'] }).present();
        });
    };
    PointPage.prototype.stopRecord = function () {
        this.audio.stopRecord();
        this.recording = false;
    };
    PointPage.prototype.send = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, _a, err_3;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(this.text !== '' && !this.touchpoint)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _b.sent();
                        _b.label = 2;
                    case 2:
                        if (!(this.text !== '')) return [3 /*break*/, 6];
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        payload = { content: this.text };
                        _a = this;
                        return [4 /*yield*/, this.restangular.one('touchpoints', this.touchpoint.id).all('texts').customPOST(payload).toPromise()];
                    case 4:
                        _a.touchpoint = _b.sent();
                        this.text = '';
                        this.api.getUserJourney(this.user_journey.id);
                        this.autoSize(40);
                        return [3 /*break*/, 6];
                    case 5:
                        err_3 = _b.sent();
                        console.log(err_3);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.sendAudio = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, fileTransfer, options, data, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.touchpoint) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        loading = this.loadingCtrl.create({ content: 'Please wait...' });
                        loading.present();
                        fileTransfer = this.transfer.create();
                        options = {
                            fileKey: 'file',
                            mimeType: 'audio/mp3',
                            fileName: this.fileName,
                            headers: { Authorization: "Bearer " + this.auth.token },
                            params: { type: 'audio' }
                        };
                        console.log(options);
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, 6, 7]);
                        return [4 /*yield*/, fileTransfer.upload(this.filePath, __WEBPACK_IMPORTED_MODULE_6__config_config_dev__["a" /* ENV */].API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options)];
                    case 4:
                        data = _a.sent();
                        console.log(JSON.parse(data.response));
                        this.api.getUserJourney(this.user_journey.id);
                        return [3 /*break*/, 7];
                    case 5:
                        err_4 = _a.sent();
                        console.log(err_4);
                        return [3 /*break*/, 7];
                    case 6:
                        loading.dismiss();
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.sendPicture = function (image) {
        return __awaiter(this, void 0, void 0, function () {
            var loading, fileTransfer, options, data, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('image received', image);
                        if (!!this.touchpoint) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        loading = this.loadingCtrl.create({ content: 'Please wait...' });
                        loading.present();
                        fileTransfer = this.transfer.create();
                        options = {
                            fileKey: 'file',
                            mimeType: image.type,
                            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                            headers: { Authorization: "Bearer " + this.auth.token },
                            params: { type: 'image' }
                        };
                        console.log(options);
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, 6, 7]);
                        return [4 /*yield*/, fileTransfer.upload(image.url, __WEBPACK_IMPORTED_MODULE_6__config_config_dev__["a" /* ENV */].API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options)];
                    case 4:
                        data = _a.sent();
                        console.log(JSON.parse(data.response));
                        this.api.getUserJourney(this.user_journey.id);
                        return [3 /*break*/, 7];
                    case 5:
                        err_5 = _a.sent();
                        console.log(err_5);
                        return [3 /*break*/, 7];
                    case 6:
                        loading.dismiss();
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.sendRate = function (rate) {
        return __awaiter(this, void 0, void 0, function () {
            var payload, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.touchpoint) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        payload = { mark: rate };
                        return [4 /*yield*/, this.restangular.one('touchpoints', this.touchpoint.id).customPATCH(payload).toPromise()];
                    case 3:
                        _a.sent();
                        this.api.getUserJourney(this.user_journey.id);
                        return [3 /*break*/, 5];
                    case 4:
                        err_6 = _a.sent();
                        console.log(err_6);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.sendVideo = function (video) {
        return __awaiter(this, void 0, void 0, function () {
            var fileTransfer, options, data, err_7;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('video received', video);
                        if (!!this.touchpoint) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.addTouchpoint()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        fileTransfer = this.transfer.create();
                        fileTransfer.onProgress(function (event) {
                            if (event.lengthComputable) {
                                var progress = Math.round((event.loaded / event.total) * 100);
                                console.log('Progress', progress);
                                _this.utils.setUploadingProgress(progress);
                            }
                        });
                        options = {
                            fileKey: 'file',
                            mimeType: video.type,
                            fileName: video.name,
                            headers: { Authorization: "Bearer " + this.auth.token },
                            params: { type: 'video' }
                        };
                        console.log(options);
                        this.uploading = true;
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, 6, 7]);
                        return [4 /*yield*/, fileTransfer.upload(video.fullPath, __WEBPACK_IMPORTED_MODULE_6__config_config_dev__["a" /* ENV */].API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options)];
                    case 4:
                        data = _a.sent();
                        console.log(JSON.parse(data.response));
                        this.api.getUserJourney(this.user_journey.id);
                        return [3 /*break*/, 7];
                    case 5:
                        err_7 = _a.sent();
                        console.log(err_7);
                        return [3 /*break*/, 7];
                    case 6:
                        this.uploading = false;
                        this.utils.setUploadingProgress(0);
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.deleteTouchpointContent = function (touchpoint, index) {
        return __awaiter(this, void 0, void 0, function () {
            var answer;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(index, touchpoint[index].type, this.touchpoint);
                        return [4 /*yield*/, this.restangular.all('deleteTouchpointContent').customPOST({ media_id: touchpoint[index].id, media_type: touchpoint[index].type }).toPromise()];
                    case 1:
                        answer = _a.sent();
                        console.log('answer ', answer);
                        this.touchpoint.content.splice(index, 1);
                        return [2 /*return*/];
                }
            });
        });
    };
    PointPage.prototype.getCurrentImage = function () {
        var _this = this;
        console.log("getCurrentImage ");
        this.storage.get('journeys').then(function (val) {
            var id = _this.user_journey['journey_id'];
            for (var _i = 0, val_1 = val; _i < val_1.length; _i++) {
                var i = val_1[_i];
                if (i['id'] == _this.user_journey['journey_id'])
                    _this.currentImage = i['image'];
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], PointPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */])
    ], PointPage.prototype, "navBar", void 0);
    PointPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-point',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\point\point.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="secondary" (click)="goBack()">\n\n        <ion-title>{{"POINT.WHERE" | translate}}</ion-title>\n\n        <load-bar></load-bar>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content class="point-background"  [ngStyle]="{\'background-image\':\'url(\' + currentImage + \')\'}"   *ngIf="point">\n\n\n\n    <ion-item no-lines [ngStyle]="{\'background-color\': point.color}">\n\n        {{point.title}}\n\n    </ion-item>\n\n\n\n    <div class="div90" text-left *ngIf="touchpoint && touchpoint.content && touchpoint.content.length">\n\n        <div *ngFor="let item of touchpoint.content let i=index">\n\n            <div *ngIf="item.type === \'text\'">\n\n                <div ion-item no-lines class="content-wrapper">\n\n                    {{item.content}}\n\n                    <div item-end>\n\n                        <ion-icon name="ios-trash-outline" color="success"  (click)="deleteTouchpointContent(touchpoint.content,i)"></ion-icon>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n            <div *ngIf="item.type === \'image\'">\n\n                <div class="image-wrapper">\n\n                    <img [src]="item.fullUrl" class="content-image" *ngIf="item.fullUrl">\n\n                    <div item-end>\n\n                        <ion-icon name="ios-trash-outline" color="success"  (click)="deleteTouchpointContent(touchpoint.content,i)"></ion-icon>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n            <div *ngIf="item.type === \'video\'">\n\n                <div class="content-wrapper">\n\n                    <video controls controlsList="nodownload" src="{{item.fullUrl}}" class="content-video"></video>\n\n                    <div item-end>\n\n                        <ion-icon name="ios-trash-outline" color="success"  (click)="deleteTouchpointContent(touchpoint.content,i)"></ion-icon>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n            <div *ngIf="item.type === \'audio\'">\n\n                <div class="content-wrapper">\n\n                    <audio controls src="{{item.fullUrl}}" controlsList="nodownload"></audio>\n\n                    <div item-end>\n\n                        <ion-icon name="ios-trash-outline" color="success" (click)="deleteTouchpointContent(touchpoint.content,i)"></ion-icon>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n    <div class="div90" text-left *ngIf="uploading">\n\n        <div class="fake-video-wrapper">\n\n            <ion-spinner name="ios"></ion-spinner>\n\n            <div ion-text color="primary" padding-top>{{"POINT.UPLOADING" | translate}}: {{progress}}%</div>\n\n        </div>\n\n    </div>\n\n\n\n</ion-content>\n\n<ion-footer no-padding>\n\n    <ion-row align-items-center class="star-row">\n\n        <ion-col col-12 text-center>\n\n            <div ion-button clear *ngFor="let star of stars, let i = index" class="star-icon">\n\n                <ion-icon (click)="rate(i)" *ngIf="!star" name="star-outline"></ion-icon>\n\n                <ion-icon (click)="rate(i)" *ngIf="star" name="star"></ion-icon>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row align-items-center text-center *ngIf="!recording">\n\n        <ion-col col-1 class="custom-padding-left">\n\n            <ion-icon name="videocam" color="primary" add-video (targetVideo)="sendVideo($event)"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon name="mic" color="primary" (click)="record()"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon name="image" color="primary" add-picture-gallery  (targetImage)="sendPicture($event)"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon name="camera" color="primary" add-picture (targetImage)="sendPicture($event)"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-6 no-padding>\n\n            <ion-textarea (input)="autoSize();" placeholder="{{\'POINT.DOCUMENT\' | translate}}" [(ngModel)]="text"></ion-textarea>\n\n        </ion-col>\n\n        <ion-col col-2 no-padding>\n\n            <!--<button ion-button no-border no-padding clear small color="primary" (click)="send()">{{"POINT.SUBMIT" | translate}}</button>-->\n\n            <ion-icon name="ios-arrow-dropright-circle-outline" color="primary" (click)="send()" [ngClass]="{\'icon-rtl\': language === \'he\'}"></ion-icon>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row align-items-center text-center *ngIf="recording">\n\n        <ion-col col-12>\n\n            <button ion-button block (click)="stopRecord()">{{"POINT.RECORDING" | translate}}</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\point\point.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["a" /* Restangular */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_media__["a" /* Media */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */]])
    ], PointPage);
    return PointPage;
}());

//# sourceMappingURL=point.js.map

/***/ })

});
//# sourceMappingURL=0.js.map