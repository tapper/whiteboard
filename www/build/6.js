webpackJsonp([6],{

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupPageModule", function() { return PopupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popup__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PopupPageModule = /** @class */ (function () {
    function PopupPageModule() {
    }
    PopupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__popup__["a" /* PopupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__popup__["a" /* PopupPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], PopupPageModule);
    return PopupPageModule;
}());

//# sourceMappingURL=popup.module.js.map

/***/ }),

/***/ 717:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(187);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupPage = /** @class */ (function () {
    function PopupPage(navCtrl, navParams, api, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.viewCtrl = viewCtrl;
        this.journeys = [];
        this.hasHistory = false;
        this.apiSubscribe = this.api.journeys$.subscribe(function (journeys) {
            _this.journeys = journeys;
            for (var _i = 0, _a = _this.journeys; _i < _a.length; _i++) {
                var journey = _a[_i];
                if (journey.user_journeys && journey.user_journeys.length) {
                    _this.hasHistory = true;
                    break;
                }
            }
        });
        this.languageSubscribe = this.api.language$.subscribe(function (language) { return _this.language = language; });
    }
    PopupPage.prototype.ionViewDidLeave = function () {
        this.languageSubscribe.unsubscribe();
        this.apiSubscribe.unsubscribe();
    };
    PopupPage.prototype.goToJourney = function (journey) {
        this.viewCtrl.dismiss({ journeys: 'one', journey: journey });
    };
    PopupPage.prototype.goToJourneys = function (journey) {
        this.viewCtrl.dismiss({ journeys: 'many', journey: journey });
    };
    PopupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-popup',template:/*ion-inline-start:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\popup\popup.html"*/'<ion-content (click)="viewCtrl.dismiss()">\n\n\n\n    <div class="div90 content-wrapper" *ngIf="hasHistory">\n\n        <div *ngFor="let journey of journeys" [ngClass]="{\'journey-item-rtl\' : language === \'he\', \'journey-item-ltr\' : language === \'en\'}">\n\n            <div [ngClass]="{\'new-journey-rtl\' : language === \'he\', \'new-journey-ltr\' : language === \'en\'}" (click)="goToJourney(journey); $event.stopPropagation();">\n\n                {{journey.title}}\n\n            </div>\n\n            <!--<div [ngClass]="{\'journey-gap-rtl\' : language === \'he\', \'journey-gap-ltr\' : language === \'en\'}"></div>-->\n\n            <div [ngClass]="{\'journey-history-rtl\' : language === \'he\', \'journey-history-ltr\' : language === \'en\'}">\n\n                <button no-margin ion-button (click)="goToJourney(journey); $event.stopPropagation();">{{"POINT.NEW" | translate}}</button>\n\n                <button no-margin ion-button *ngIf="journey.user_journeys && journey.user_journeys.length" (click)="goToJourneys(journey); $event.stopPropagation();">{{"POINT.EDIT" | translate}}</button>\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="div90 content-wrapper" *ngIf="!hasHistory">\n\n        <div *ngFor="let journey of journeys" [ngClass]="{\'journey-item-rtl\' : language === \'he\', \'journey-item-ltr\' : language === \'en\'}">\n\n            <div [ngClass]="{\'new-journey-rtl\' : language === \'he\', \'new-journey-ltr\' : language === \'en\'}" style="width: 100%;"  (click)="goToJourney(journey); $event.stopPropagation();">\n\n                {{journey.title}}\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\gitlab\whiteboard\src\pages\popup\popup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]])
    ], PopupPage);
    return PopupPage;
}());

//# sourceMappingURL=popup.js.map

/***/ })

});
//# sourceMappingURL=6.js.map