import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Journey} from '../../providers/api/api.journey';
import {ApiProvider} from '../../providers/api/api';
import {UtilsProvider} from '../../providers/utils/utils';

@IonicPage()
@Component({
    selector: 'page-journeys',
    templateUrl: 'journeys.html',
})
export class JourneysPage {

    journey: Journey = this.navParams.get('journey');
    user_journeys = this.journey.user_journeys;
    coordinates = {lat: 0, lng: 0};
    utilsSubscribe:any;
    language: string;
    language_subscription:any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiProvider,
                public utils: UtilsProvider) {
        this.utilsSubscribe = this.utils.coordinates$.subscribe(coordinates => {this.coordinates = coordinates});
        this.language_subscription = this.api.language$.subscribe(language => this.language = language);
    }
    
    ionViewDidLeave () {
        this.utilsSubscribe.unsubscribe();
        this.language_subscription.unsubscribe();
    }

    async goToUserJourney(user_journey) {
        try {
            await this.utils.checkLocation();
        } catch (err){
            console.log(err);
        } finally {
            this.api.reportJourneyPeriod('start', user_journey.id, this.coordinates.lat, this.coordinates.lng);
            this.api.reportTouchpointPeriod('start', user_journey.id, this.coordinates.lat, this.coordinates.lng);
            this.navCtrl.setRoot('PointsPage', {user_journey: user_journey, title: this.journey.title});
        }

    }

}
