import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {Journey} from '../../providers/api/api.journey';

@IonicPage()
@Component({
    selector: 'page-popup',
    templateUrl: 'popup.html',
})
export class PopupPage {

    journeys: Array<Journey> = [];
    language: string;
    hasHistory: boolean = false;
    apiSubscribe:any;
    languageSubscribe:any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiProvider,
                public viewCtrl: ViewController) {
        this.apiSubscribe = this.api.journeys$.subscribe(journeys => {
            this.journeys = journeys;
            for (let journey of this.journeys){
                if (journey.user_journeys && journey.user_journeys.length){
                    this.hasHistory = true;
                    break;
                }
            }
        });
        this.languageSubscribe = this.api.language$.subscribe(language => this.language = language);
    }
    
    ionViewDidLeave () {
        this.languageSubscribe.unsubscribe();
        this.apiSubscribe.unsubscribe();
    }
    
    goToJourney (journey) {
        this.viewCtrl.dismiss({journeys: 'one', journey: journey});
    }

    goToJourneys (journey){
        this.viewCtrl.dismiss({journeys: 'many', journey: journey});
    }

}
