import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    user: any;
    apiSubscribe:any;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public modalCtrl: ModalController,
                public api: ApiProvider) {
        this.apiSubscribe = this.api.user$.subscribe(user => this.user = user);
    }
    
    ionViewDidLeave () {
        this.apiSubscribe.unsubscribe();
    }
    
    showPopup () {
        let modal = this.modalCtrl.create('PopupPage');
        modal.present();

        modal.onDidDismiss(data => {
            if (data){
                if (data.journeys === 'one'){
                    this.navCtrl.push('JourneyPage', {journey: data.journey});
                } else {
                    this.navCtrl.push('JourneysPage', {journey: data.journey});
                }
            }
        })
    }

    goToProfile () {
        this.navCtrl.push('ProfilePage');
    }

}
