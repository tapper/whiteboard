import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {UtilsProvider} from '../../providers/utils/utils';

@IonicPage()
@Component({
    selector: 'page-journey',
    templateUrl: 'journey.html',
})
export class JourneyPage {

    journey = this.navParams.get('journey');
    coordinates = {lat: 0, lng: 0};
    utilsSubscribe:any;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiProvider,
                public loadingCtrl: LoadingController,
                public utils: UtilsProvider) {
        this.utilsSubscribe = this.utils.coordinates$.subscribe(coordinates => {this.coordinates = coordinates});
    }
    
    ionViewDidLeave () {
        this.utilsSubscribe.unsubscribe();
    }

    async startJourney () {
        console.log(this.journey);
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            await this.utils.checkLocation();
        } catch(err){
            console.log(err);
        } finally {
            let journey = await this.api.startJourney(this.journey.id);
            this.api.reportJourneyPeriod('start', journey.id, this.coordinates.lat, this.coordinates.lng);
            this.api.reportTouchpointPeriod('start', journey.id, this.coordinates.lat, this.coordinates.lng);
            this.navCtrl.setRoot('PointsPage', {user_journey: journey, title: this.journey.title})
            loading.dismiss();
        }
    }

}
