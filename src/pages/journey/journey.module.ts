import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {DirectivesModule} from '../../directives/directives.module';
import {JourneyPage} from './journey';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    JourneyPage,
  ],
  imports: [
    IonicPageModule.forChild(JourneyPage),
      DirectivesModule,
      TranslateModule
  ],
})
export class JourneyPageModule {}
