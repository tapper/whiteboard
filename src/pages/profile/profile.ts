import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {AuthProvider} from '../../providers/auth/auth';
import {UtilsProvider} from '../../providers/utils/utils';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer';
import {ENV} from '../../config/config.dev';
import {ApiProvider} from '../../providers/api/api';
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {

    user: any;
    form: FormGroup = this.fb.group({
        name: [''],
        phone: [''],
        email: ['', [Validators.required, Validators.email]],
    });

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiProvider,
                public alertCtrl: AlertController,
                public utils: UtilsProvider,
                public fb: FormBuilder,
                public auth: AuthProvider,
                public translate: TranslateService,
                public transfer: FileTransfer,
                public loadingCtrl: LoadingController,
                public restangular: Restangular) {
        this.api.user$.subscribe(user => {
            this.user = user;
            this.form.setValue({name: this.user.name, email: this.user.email, phone: this.user.phone})
        });
    }

    showDeletePopup () {
        let alert = this.alertCtrl.create({
            title: this.translate.instant('ALERTS.SURE'),
            message: this.translate.instant('ALERTS.DELETEAVATAR'),
            buttons: [
                {text: 'Cancel', role: 'cancel'},
                {
                    text: 'OK',
                    handler: () => {this.deleteAvatar()}
                }
            ]
        });
        alert.present();
    }

    async deleteAvatar () {
        for (let item of this.user.media){
            if (item.pivot.tag === 'avatar'){
                try {
                    await this.restangular.one('media/avatar', item.id).remove().toPromise();
                    this.alertCtrl.create({title: this.translate.instant('ALERTS.DELETED'), buttons: ['OK']}).present();
                    this.api.getUser();
                } catch (err) {
                    console.log(err);
                }
            }
        }
    }

    async sendPicture(image: any){

        console.log('image received', image);

        try {

            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();

            const fileTransfer: FileTransferObject = this.transfer.create();

            let options: FileUploadOptions = {
                fileKey: 'file',
                mimeType: image.type,
                fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                headers: {Authorization: `Bearer ${this.auth.token}`},
                params: {type: 'image', user_id: this.user.id}
            };

            console.log(options);

            try {
                let data = await fileTransfer.upload(image.url, ENV.API_ENDPOINT + '/media/avatar', options);
                console.log(JSON.parse(data.response));
                this.api.getUser();
            } catch (err){
                console.log(err);
            } finally {
                loading.dismiss();
            }
        } catch (err){
            console.log(err);
        }

    }

    async submit () {
        if (this.form.invalid){
            return;
        }

        try {
            this.user.name = this.form.value.name;
            this.user.phone = this.form.value.phone;
            this.user.email = this.form.value.email;
            await this.user.patch().toPromise();
            this.api.getUser();
            this.alertCtrl.create({title: this.translate.instant('ALERTS.UPDATED'), buttons: ['OK']}).present();
        } catch (err){
            console.log(err);
        }
    }

}
