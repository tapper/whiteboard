import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {
    AlertController, Content, IonicPage, LoadingController, Navbar, NavController, NavParams, Platform
} from 'ionic-angular';
import {Point} from '../../providers/api/api.point';
import {Restangular} from 'ngx-restangular';
import {ApiProvider} from '../../providers/api/api';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer';
import {AuthProvider} from '../../providers/auth/auth';
import {ENV} from '../../config/config.dev';
import {UtilsProvider} from '../../providers/utils/utils';
import {Media, MediaObject} from '@ionic-native/media';
import {File} from '@ionic-native/file';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-point',
    templateUrl: 'point.html',
})
export class PointPage {

    @ViewChild(Content) content: Content;
    @ViewChild(Navbar) navBar: Navbar;

    audio: MediaObject;
    coordinates = {lat: 0, lng: 0};
    fileName: string;
    filePath: string;
    point: Point = this.navParams.get('point');
    progress: number = 0;
    recording: boolean = false;
    stars: Array<boolean> = [false, false, false, false, false];
    text: string = '';
    touchpoint;
    uploading: boolean = false;
    user_journey: any;
    currentImage:string;
    journeys;
    apiSubscribe;
    uploadSubscribe;
    coordinatesSubscribe;
    audioSuccessSubscribe;
    audioErrorSubscribe;
    language: string;
    language_subscription:any;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public api: ApiProvider,
                public transfer: FileTransfer,
                public auth: AuthProvider,
                public loadingCtrl: LoadingController,
                public utils: UtilsProvider,
                private zone: NgZone,
                public alertCtrl: AlertController,
                public media: Media,
                public platform: Platform,
                public translate: TranslateService,
                public element: ElementRef,
                private file: File,
                public storage: Storage) {
    
    
        this.apiSubscribe = this.api.user_journey$.subscribe(journey => {
            if (journey){
                this.user_journey = journey;
                console.log("UserJ : " , this.user_journey);
                
                if (this.user_journey.touchpoints && this.user_journey.touchpoints.length){
                    for (let touchpoint of this.user_journey.touchpoints){
                        if (touchpoint.point_id === this.point.id){
                            this.touchpoint = touchpoint;
                        }
                    }
                }
            }
            
            this.language_subscription = this.api.language$.subscribe(language => this.language = language);
            this.getCurrentImage();
        });
    
        
    
        // this.api.journeys$.subscribe(journeys => {
        //     this.journeys = journeys;
        //     for (let journey of this.journeys){
        //         if (journey.user_journeys && journey.user_journeys.length){
        //             this.hasHistory = true;
        //             break;
        //         }
        //     }
        // });

        this.platform.registerBackButtonAction(() => {
            this.goBack();
            return;
        });
    
        this.uploadSubscribe = this.utils.uploadingProgress$.subscribe(progress => {this.zone.run(() => {this.progress = progress;});});
    
        this.coordinatesSubscribe =  this.utils.coordinates$.subscribe(coordinates => {this.coordinates = coordinates});
        
    }
    
    ionViewDidLeave () {
        this.apiSubscribe.unsubscribe();
        this.uploadSubscribe.unsubscribe();
        this.coordinatesSubscribe.unsubscribe();
        
        if(this.audioSuccessSubscribe)
        this.audioSuccessSubscribe.unsubscribe();
        if(this.audioErrorSubscribe)
        this.audioErrorSubscribe.unsubscribe();
    }
    
    async goBack() {
        if (this.touchpoint){
            await this.utils.checkLocation();
            this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng);
        }
        this.navCtrl.pop();
    }

    protected autoSize(pixels: number = 0): void {
        let textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        textArea.style.overflow = 'hidden';
        textArea.style.height 	= 'auto';
        if (pixels === 0){
            textArea.style.height 	= textArea.scrollHeight + 'px';
        } else {
            textArea.style.height 	= pixels + 'px';
        }
        return;

    }

    async editTouchpoint (): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.utils.checkLocation();
                this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng, this.touchpoint.id);
                resolve();
            } catch (err){
                console.log(err);
                reject();
            }
        })
    }

    async addTouchpoint (): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.utils.checkLocation();
                let payload = {
                    user_journey_id: this.user_journey.id,
                    point_id: this.point.id,
                    lat: this.coordinates.lat,
                    lng: this.coordinates.lng
                };
                this.touchpoint = await this.restangular.all('touchpoints').customPOST(payload).toPromise();
                await this.api.reportTouchpointPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng, this.touchpoint.id);
                resolve();
            } catch (err){
                console.log(err);
                reject();
            }
        })
    }

    ionViewWillEnter () {
        this.user_journey = this.navParams.get('user_journey');
        this.point = this.navParams.get('point');
        this.touchpoint = this.point.touchpoint;
        if (this.touchpoint && this.touchpoint.mark){
            for (let i = 0; i < this.stars.length; i++){
                if (i < this.touchpoint.mark){
                    this.stars[i] = true;
                }
                else if (i >= this.touchpoint.mark && i < this.stars.length) {
                    this.stars[i] = false;
                }
            }
        }
        console.log(this.touchpoint);
        this.content.scrollToBottom();
    }

    ionViewDidEnter () {
        this.navBar.backButtonClick = () => {
            event.stopPropagation();
            this.goBack();
        };
        if (this.touchpoint){
            this.editTouchpoint();
        }
    }

    rate (index: number): void {
        for (let i = 0; i < this.stars.length; i++){
            if (i <= index){
                this.stars[i] = true;
            }
            else if (i > index && i < this.stars.length) {
                this.stars[i] = false;
            }
        }
        this.sendRate(index + 1);
    }

    record () {

        if (this.platform.is('ios')) {
            this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.mp3';
            this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
        } else if (this.platform.is('android')) {
            this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.mp3';
            this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
        }

        this.audio = this.media.create(this.filePath);
        this.audio.startRecord();
        this.recording = true;
       
        this.audioSuccessSubscribe = this.audio.onSuccess.subscribe(() => {
            console.log('Action is successful');
            this.sendAudio();
        });
    
        this.audioErrorSubscribe = this.audio.onError.subscribe(error => {
            console.log('Error!', error);
            this.alertCtrl.create({title: this.translate.instant('ALERTS.RECORD'), buttons: ['OK']}).present();
        });

    }

    stopRecord() {
        this.audio.stopRecord();
        this.recording = false;
    }

    async send () {

        if (this.text !== '' && !this.touchpoint){
            await this.addTouchpoint();
        }

        if (this.text !== ''){
            try {
                let payload = {content: this.text};
                this.touchpoint = await this.restangular.one('touchpoints', this.touchpoint.id).all('texts').customPOST(payload).toPromise();
                this.text = '';
                this.api.getUserJourney(this.user_journey.id);
                this.autoSize(40);
            } catch (err){
                console.log(err);
            }
        }

    }

    async sendAudio () {

        if (!this.touchpoint){
            await this.addTouchpoint();
        }

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'file',
            mimeType: 'audio/mp3',
            fileName: this.fileName,
            headers: {Authorization: `Bearer ${this.auth.token}`},
            params: {type: 'audio'}
        };

        console.log(options);

        try {
            let data = await fileTransfer.upload(this.filePath, ENV.API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options);
            console.log(JSON.parse(data.response));
            this.api.getUserJourney(this.user_journey.id);
        } catch (err){
            console.log(err);
        } finally {
            loading.dismiss();
        }

    }

    async sendPicture(image: any) {

        console.log('image received', image);

        if (!this.touchpoint){
            await this.addTouchpoint();
        }

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'file',
            mimeType: image.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: {Authorization: `Bearer ${this.auth.token}`},
            params: {type: 'image'}
        };

        console.log(options);

        try {
            let data = await fileTransfer.upload(image.url, ENV.API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options);
            console.log(JSON.parse(data.response));
            this.api.getUserJourney(this.user_journey.id);
        } catch (err){
            console.log(err);
        } finally {
            loading.dismiss();
        }
    }

    async sendRate (rate: number) {

        if (!this.touchpoint){
            await this.addTouchpoint();
        }

        try {
            let payload = {mark: rate};
            await this.restangular.one('touchpoints', this.touchpoint.id).customPATCH(payload).toPromise();
            this.api.getUserJourney(this.user_journey.id);
        } catch (err){
            console.log(err);
        }

    }

    async sendVideo(video: any) {

        console.log('video received', video);

        if (!this.touchpoint){
            await this.addTouchpoint();
        }

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {
                let progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                this.utils.setUploadingProgress(progress);
            }

        });

        let options: FileUploadOptions = {
            fileKey: 'file',
            mimeType: video.type,
            fileName: video.name,
            headers: {Authorization: `Bearer ${this.auth.token}`},
            params: {type: 'video'}
        };

        console.log(options);

        this.uploading = true;

        try {
            let data = await fileTransfer.upload(video.fullPath, ENV.API_ENDPOINT + '/touchpoints/' + this.touchpoint.id + '/media', options);
            console.log(JSON.parse(data.response));
            this.api.getUserJourney(this.user_journey.id);
        } catch (err){
            console.log(err);
        } finally {
            this.uploading = false;
            this.utils.setUploadingProgress(0);
        }

    }
    
    async deleteTouchpointContent(touchpoint,index)
    {
        console.log(index , touchpoint[index].type , this.touchpoint);
        let answer = await this.restangular.all('deleteTouchpointContent').customPOST({media_id: touchpoint[index].id,media_type:touchpoint[index].type}).toPromise();
        console.log('answer ' , answer)
        this.touchpoint.content.splice(index,1)
    }
    
    getCurrentImage()
    {
        console.log("getCurrentImage " )
        this.storage.get('journeys').then((val) => {
            let id = this.user_journey['journey_id'];
            
            for(let i of val)
            {
                if(i['id'] == this.user_journey['journey_id'])
                    this.currentImage = i['image'];
            }
        });
    }
    
}
