import {Component, Injectable, ViewChild} from '@angular/core';
import {AlertController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthProvider} from '../providers/auth/auth';
import {AuthStatuses} from '../providers/auth/auth.status';
import {ApiProvider} from '../providers/api/api';
import {UtilsProvider} from '../providers/utils/utils';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import {Keyboard} from '@ionic-native/keyboard';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = 'HomePage';
    showFooter: boolean = true;
    imagesArray:any[] = [];

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                public auth: AuthProvider,
                public api: ApiProvider,
                public utils: UtilsProvider,
                public translate: TranslateService,
                public storage: Storage,
                public keyboard: Keyboard,
                public alertCtrl: AlertController) {
        this.initializeApp();
    }





    async initializeApp() {

        await this.platform.ready();

        this.statusBar.styleDefault();

        let language = await this.storage.get('language');
        if (language == null){
            language = 'en';
        }

        console.log(language);
        this.api.setLanguage(language);
        this.translate.setDefaultLang(language);
        this.platform.setDir(language === 'en' ? 'ltr' : 'rtl', true);

        try {
            await this.auth.checkTokenOnStart();
        } catch (err){
            console.log('checkTokenOnStart', err);
        }

        this.auth.status$.subscribe(async (status) => {
            console.log("Status : " , status , AuthStatuses.AUTHENTICATED)
            switch (status) {
                case AuthStatuses.AUTHENTICATED:
                    this.utils.getLocation();
                    console.log("LLL1");
                    
                    let a = await this.api.getJourneys();
                    for(let image of a)
                    {
                        let obj = {
                            id:image['id'],
                            image:image['imagetwo']
                        }
    
                        this.imagesArray.push(obj);
                    }
                    
                    console.log("II " , this.imagesArray)
                    await this.storage.set('journeys',this.imagesArray);
                    
                    console.log("TTT : " , a);
                    await this.api.getUser();
                    // this.nav.setRoot('HomePage');
                    this.splashScreen.hide();
                    break;
                case AuthStatuses.NOT_AUTHENTICATED:
                    await this.nav.setRoot('LoginPage');
                    this.splashScreen.hide();
                    break;
            }
        });

    }

    logout () {
        this.auth.logout();
        this.nav.setRoot('LoginPage');
    }

    async changeLanguage () {
        let lang = await this.storage.get('language');

        let alert = this.alertCtrl.create();
        alert.setTitle("Select language");

        alert.addInput({type: 'radio', label: 'English', value: 'en', checked: lang === 'en'});
        alert.addInput({type: 'radio', label: 'עברית', value: 'he', checked: lang === 'he'});

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.api.setLanguage(data).then(response => {
                    this.api.getJourneys();
                });
                this.translate.setDefaultLang(data);
                this.platform.setDir(data === 'en' ? 'ltr' : 'rtl', true);
            }
        });
        alert.present();
    }

    goToProfile () {
        this.nav.push('ProfilePage');
    }
}
