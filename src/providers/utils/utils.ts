import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Diagnostic} from '@ionic-native/diagnostic';
import {AlertController, LoadingController, Platform, ToastController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class UtilsProvider {

    private _uploadingProgress: BehaviorSubject<number> = new BehaviorSubject(0);
    readonly uploadingProgress$: Observable<number> = this._uploadingProgress.asObservable();
    get uploadingProgress(): number {return this._uploadingProgress.getValue();}

    private _coordinates: BehaviorSubject<{lat: number, lng: number}> = new BehaviorSubject({lat: 0, lng: 0});
    readonly coordinates$: Observable<{lat: number, lng: number}> = this._coordinates.asObservable();
    get coordinates(): {lat: number, lng: number} {return this._coordinates.getValue();}
    public locationCounter:number = 0;
    public locationCounter1:number = 0;
    public locationCounter2:number = 0;
    
    constructor(public diagnostic: Diagnostic,
                public platform: Platform,
                public geolocation: Geolocation,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController) {}

    setUploadingProgress(value: number) {
        this._uploadingProgress.next(value);
    }

    async isLocationAvailable (): Promise<any> {

        return new Promise (async (resolve, reject) => {

            let isAuthorized = await this.diagnostic.isLocationAuthorized();
            console.log('isLocationAuthorized', isAuthorized);

            if (!isAuthorized) {

                let status = await this.diagnostic.requestLocationAuthorization();
                console.log('requestLocationAuthorization', status);

                if (status === "GRANTED"){

                    let isEnabled = await this.diagnostic.isLocationEnabled();
                    console.log('isLocationEnabled', isEnabled);

                    if (isEnabled){
                        resolve({status: 'success'});
                    } else {
                        reject({status: 'error', message: this.translate.instant('ALERTS.DISABLED')});
                    }

                } else {
                    reject({status: 'error', message: this.translate.instant('ALERTS.AUTHORIZED')});
                }

            } else {

                let isEnabled = await this.diagnostic.isLocationEnabled();
                console.log('isLocationEnabled', isEnabled);

                if (isEnabled){
                    resolve({status: 'success'});
                } else {
                    reject({status: 'error', message: this.translate.instant('ALERTS.DISABLED')});
                }

            }

        })

    }

    async getLocation () {

        return new Promise (async (resolve, reject) => {

            if (this.platform.is('cordova')) {   // for phones

                try {
                    await this.isLocationAvailable();

                    try {
                        let response = await this.geolocation.getCurrentPosition({timeout: 500});
                        console.log(response.coords.latitude, response.coords.longitude);
                        this._coordinates.next({lat: response.coords.latitude, lng: response.coords.longitude});
                        resolve();
                    } catch (err) {
                        if(this.locationCounter1 == 0) {
                            this.toastCtrl.create({
                                message: this.translate.instant('ALERTS.CANTGET'),
                                position: 'bottom',
                                showCloseButton: true
                            }).present();
                            this.locationCounter1++
                        }
                            reject();
                    }
                } catch (err) {
                    console.log(err);
                    if(this.locationCounter1 == 0) {
                        this.toastCtrl.create({message: err.message, position: 'bottom', showCloseButton: true}).present();
                        this.locationCounter1++
                    }
                    reject();
                }

            } else {        // for browser
                try {
                    let response = await this.geolocation.getCurrentPosition({maximumAge: 10000, timeout: 5000});
                    console.log(response.coords.latitude, response.coords.longitude);
                    this._coordinates.next({lat: response.coords.latitude, lng: response.coords.longitude});
                    resolve();
                } catch (err) {
                    this.toastCtrl.create({message: err.message, position: 'bottom', showCloseButton: true}).present();
                    this.locationCounter1++
                    reject();
                }

            }
        })
    }

    async checkLocation () {
        return new Promise (async (resolve, reject) => {
            //let loading = this.loadingCtrl.create({content: 'Please wait...'});
            //loading.present();
            try {
                await this.getLocation();
                resolve();
            } catch (err){
                console.log(err);
                if(this.locationCounter == 0)
                {
                    let alert = this.alertCtrl.create({
                        title: this.translate.instant('ALERTS.NOLOCATION'),
                        message: this.translate.instant('ALERTS.CHECK'),
                        buttons: [
                            {
                                text: this.translate.instant('ALERTS.CONTINUE'),
                                handler: () => {resolve()}
                            },
                            {
                                text: this.translate.instant('ALERTS.TRYAGAIN'),
                                handler: () => {
                                    this.getLocation().then(success => {resolve();}, error => {reject();});
                                }
                            },
                        ]
                    });
                    alert.present();
                    this.locationCounter++
                }
                else {
                    resolve();
                }
            } finally {
                //loading.dismiss();
            }
        })
    }

}
