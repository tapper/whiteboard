import {Injectable, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Journey} from './api.journey';
import {Restangular} from 'ngx-restangular';
import {LoadingController, Nav, NavController, NavParams} from 'ionic-angular';
import {Storage} from '@ionic/storage';


@Injectable()
export class ApiProvider {
    @ViewChild('content') nav: NavController;
    
    private _language: BehaviorSubject<string> = new BehaviorSubject(null);
    get language(): string {return this._language.getValue();}
    readonly language$: Observable<string> = this._language.asObservable();

    private _journeys: BehaviorSubject<Array<Journey>> = new BehaviorSubject(null);
    readonly journeys$: Observable<Array<Journey>> = this._journeys.asObservable();
    get journeys() : Array<Journey> {return this._journeys.getValue();}

    private _user_journey: BehaviorSubject<Journey> = new BehaviorSubject(null);
    readonly user_journey$: Observable<Journey> = this._user_journey.asObservable();
    get user_journey() : Journey {return this._user_journey.getValue();}

    private _user: BehaviorSubject<any> = new BehaviorSubject(null);
    readonly user$: Observable<any> = this._user.asObservable();
    get user() : any {return this._user.getValue();}

    constructor(public restangular: Restangular,
                public loadingCtrl: LoadingController,
                public storage: Storage) {}

    async getJourneys(): Promise<Array<Journey>> {

        return new Promise<Array<Journey>>(async (resolve, reject) => {

            //let loading = this.loadingCtrl.create({content: 'Please wait...'});
            //loading.present();

            try {

                let journeys = await this.restangular.all('journeys').getList().toPromise();
                this._journeys.next(journeys);
                resolve(this.journeys);

            } catch (err) {
                reject(err);
                console.log(err);
                // let errMessage = JSON.parse(err._body).message
                // if(errMessage == "Unauthenticated.")
                // {
                //    return "Unauthenticated"
                // }
            } finally {
                //loading.dismiss();
            }

        });

    }

    async startJourney (journey_id: number) {
        return new Promise<any>(async (resolve, reject) => {
            console.log(journey_id);
            try {
                let user_journey = await this.restangular.all('user_journeys').customPOST({journey_id: journey_id}).toPromise();
                this._user_journey.next(user_journey);
                resolve(user_journey);
            } catch (err){
                console.log(err);
            }
        });
    }


    async getUserJourney (id: number) {
        return new Promise<any>(async (resolve, reject) => {
            try {
                let user_journey = await this.restangular.one('user_journeys', id).get().toPromise();
                this._user_journey.next(user_journey);
                resolve(user_journey);
            } catch (err){
                console.log(err);
            }
        });
    }

    async getUser () {
        return new Promise<any>(async (resolve) => {
            try {
                let id = await this.storage.get('id');
                let user = await this.restangular.one('users', id).get().toPromise();
                this._user.next(user);
                resolve(user);
            } catch (err){
                console.log(err);
            }
        });
    }

    /**
     *
     * @param {string} language
     * @returns {Promise<void>}
     */
    async setLanguage(language: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            this._language.next(language);
            await this.storage.set('language', this._language.getValue());
            try {
                await this.restangular.all('lang').customGET('', {language: language}).toPromise();
                resolve();
            } catch (err){
                console.log(err);
                reject();
            }
        });
    }

    async reportTouchpointPeriod(time: string, user_journey: number, lat: number, lng: number, touchpoint: number = null, next: boolean = true){
        try {
            await this.restangular.one('user_journeys', user_journey)
                        .all('touchpoint_periods')
                        .customPOST({touchpoint_id: touchpoint, time: time, lat: lat, lng: lng, next: next})
                        .toPromise();
        } catch (err) {
            console.log(err);
        }
    }

    async reportJourneyPeriod(time: string, user_journey: number, lat: number, lng: number){
        try {
            await this.restangular.one('user_journeys', user_journey)
                        .all('journey_periods')
                        .customPOST({time: time, lat: lat, lng: lng})
                        .toPromise();
        } catch (err) {
            console.log(err);
        }
    }

}
