import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {AuthStatuses} from './auth.status';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {App} from 'ionic-angular';

@Injectable()
export class AuthProvider {

    private _token: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    get token(): string {return this._token.getValue();}
    readonly token$: Observable<string> = this._token.asObservable();

    private _id: BehaviorSubject<number> = new BehaviorSubject<number>(null);
    get id(): number {return this._id.getValue();}
    readonly id$: Observable<number> = this._id.asObservable();

    private _status: BehaviorSubject<AuthStatuses> = new BehaviorSubject(null);
    get status(): AuthStatuses {return this._status.getValue();}
    readonly status$: Observable<AuthStatuses> = this._status.asObservable();

    constructor(public storage: Storage, public app: App) {}

    async checkTokenOnStart(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let token: string = await this.storage.get('token');
                if (!token) {
                    this._status.next(AuthStatuses.NOT_AUTHENTICATED);
                } else {
                    this._token.next(token);
                    this._status.next(AuthStatuses.AUTHENTICATED);
                    let id = await this.storage.get('id');
                    this._id.next(id);
                }
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     *
     * @param {string} token
     * @returns {Promise<void>}
     */
    async setToken(token: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                this._token.next(token);
                this.storage.set('token', this._token.getValue());
                resolve();
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }

    async setId(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                this._id.next(id);
                this.storage.set('id', this._id.getValue());
                resolve();
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }


    /**
     *
     * @returns {Promise<void>}
     */
    async setStatus(status): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                this._status.next(status);
                resolve();
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }


    /**
     * Logout clearing all.
     *
     * @returns {Promise<void>}
     */
    async logout(): Promise<void> {
        return new Promise<void>(async (resolve) => {
            this.setToken(null);
            await this.storage.clear();
            this._status.next(AuthStatuses.NOT_AUTHENTICATED);
            resolve();
        });
    }

}
