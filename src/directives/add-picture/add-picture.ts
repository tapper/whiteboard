import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {AlertController} from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {TranslateService} from '@ngx-translate/core';


@Directive({
    selector: '[add-picture]' // Attribute selector
})
export class AddPictureDirective {

    @Output('targetImage')
    targetImage: EventEmitter<object> = new EventEmitter();

    constructor(public alertCtrl: AlertController,
                public translate: TranslateService,
                public camera: Camera) {}

    @HostListener('click', ['$event'])

    onClick(event: Event) {


        this.makeLogo(1);

        // let alert = this.alertCtrl.create({
        //     title: this.translate.instant("ALERTS.CHOOSESOURCE"),
        //     buttons: [
        //         {
        //             text: 'Folder',
        //             handler: () => {
        //                 this.makeLogo(0);
        //             }
        //         },
        //         {
        //             text: 'Camera',
        //             handler: () => {
        //                 this.makeLogo(1);
        //             }
        //         }
        //     ]
        // });
        // alert.present();

    }

    makeLogo(x: number) {

        let options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };

        this.camera.getPicture(options).then((imageData) => {

            console.log(imageData);
            // this.logo = imageData;
            this.targetImage.emit({url: imageData, type: 'image/jpeg'});

        }, (err) => {
            console.log('err', err);
        });

    }
}
