import {NgModule} from '@angular/core';
import {BackgroundImageDirective} from './background-image/background-image';
import {AddPictureDirective} from './add-picture/add-picture';
import {AddVideoDirective} from './add-video/add-video';
import { AddPictureGalleryDirective } from './add-picture-gallery/add-picture-gallery';

@NgModule({
	declarations: [BackgroundImageDirective,
    AddPictureDirective,
    AddVideoDirective,
    AddPictureGalleryDirective],
	imports: [],
	exports: [BackgroundImageDirective,
    AddPictureDirective,
    AddVideoDirective,
    AddPictureGalleryDirective]
})
export class DirectivesModule {}
